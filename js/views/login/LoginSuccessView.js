define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'views/common/NavigationFooterView',
  'text!templates/login/loginSuccessTemplate.html'
], function(
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    NavigationFooterView,
    LoginSuccessTemplate
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {
            _.bindAll(this);
        },

        render: function (eventName) {
            console.log("loginSuccess");

            var template  = Handlebars.compile(LoginSuccessTemplate);
            var html = template({user:this.model.toJSON()});
            this.$el.html(html);

            this.$("#profile-hexagon").empty();

            var drawContainer = d3.select(this.$("#profile-hexagon")[0]);

            var width =         120;
            var height =        120;
            var hexRadius =     52;

            var svgContainer = drawContainer.append("svg")
                .attr("width",width)
                .attr("height",height);

            //Helper function for constructing lines
            var lineFunction = d3.svg.line()
                .x(function(d) { return d.x; })
                .y(function(d) { return d.y; })
                .interpolate("linear");

            //Set clipping path
            var defs = svgContainer.append('svg:defs');
            var that = this;
            var hexagonClipPath = defs.append('clipPath')
                .attr('id','hex-clip-path')
                .append("path")
                .attr("d", function(d){
                    var x = width/2;
                    var y = width/2;
                    var points = that.hexagonPoints(x,y,hexRadius);
                    return lineFunction(points)+"Z";
                });



           var baseLayer = svgContainer.append("image")
                    .attr('xlink:href', this.model.get("userImageUrl"))
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  width)
                    .attr('height', height)
                    .attr("clip-path","url(#hex-clip-path)");


            var profileHexgon;
            if (this.model.get('gender') == 'female') {
                profileHexgon = "images/hexagon-profile-female.svg";
            } else {
                profileHexgon = "images/hexagon-profile-male.svg";
            }

            var overlay = svgContainer.append("image")
                    .attr('xlink:href', profileHexgon)
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  width)
                    .attr('height', height);

            this.loadSubviews();
        },

        //Generate array of hexagon points given center X and Y and a radius
        hexagonPoints: function(centerX, centerY, radius){
            var angleStep = 2*Math.PI/6;
            var points = [];
            for(var i = 0; i<6; i++)
            {
                var x = centerX + Math.cos(angleStep*i+angleStep/2)*radius;
                var y = centerY + Math.sin(angleStep*i+angleStep/2)*radius;
                var point = [];
                point["x"] = x;
                point["y"] = y;
                points[i]=point;
            }
            return points;
        },

        loadSubviews: function () {
            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    false,
                nextButtonTitle:    "+++++ Design a drink +++++",
                nextAllowed:        true,
                nextHref:           "#ingredients"
            });
            this.navigationFooterView.render();
        }


    });

});