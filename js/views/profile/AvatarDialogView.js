define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!templates/profile/avatarDialogTemplate.html',
], function (
    $,
    _,
    Backbone,
    Handlebars,
    AvatarDialogTemplate) {

    "use strict";

    return Backbone.View.extend({

        events: {
            "click        .avatar-item" :"avatarSelected"
        },

        initialize: function () {
        },

        render: function (eventName) {
            var template  = Handlebars.compile(AvatarDialogTemplate);
            this.$el.html(template);
        },

        loadSubViews: function() {
           
        },

        avatarSelected: function (event) {
            console.log(JSON.stringify(this.model));
            var id = $(event.currentTarget).attr('id')

            // FIXME: Hard code for now.
            var homePath = 'http://www.makrshakr.com/app/';

            var userImageUrl = homePath +'images/userImages/' + id + '.png';
            // console.log('>>>>>>>>>>>>>>>>' + userImageUrl);

            this.model.set('userImageUrl', userImageUrl);

            console.log(JSON.stringify(this.model));
        
            window.EventDispatcher.trigger("avatarDialog:close", '');
        }

    });

});
