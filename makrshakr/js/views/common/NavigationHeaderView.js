define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/common/navigationHeaderTemplate.html',
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    NavigationHeaderTemplate
) {

    "use strict";

     

    return Backbone.View.extend({
        navElems: null,

        initialize: function (options) {
           this.title = options.title;
           this.progress = options.progress;
           this.nav = options.nav || null;
        },



        render: function () {
            console.log("Render navigation header");

            var template    = Handlebars.compile(NavigationHeaderTemplate),
                el          = template({
                                title: this.title,
                                progress: this.progress,
                                nav: this.nav
                            });

            this.$el.replaceWith(el);
            this.setElement(el);
        }
    });

});