define([
    'jquery',
    'underscore',
    'backbone',
    'models/explore/PlottableRecipeModel'
], function ($, _, Backbone, PlottableRecipeModel) {

    "use strict";

    return Backbone.Collection.extend({

        model: PlottableRecipeModel,

        //specify url endpoint
        url: function () {
            return "proxy/kukaproxy.php?path=getFilteredOrders";
        },

        //specify root to models
        parse: function(resp, xhr) {
            return resp.PlottableRecipes;
        },

        fetch: function (options) {
            options.type = 'POST';
            options.data =  JSON.stringify({'minAge': 18, 'maxAge': 100 });

            //Call Backbone's fetch
            return Backbone.Collection.prototype.fetch.call(this, options);
        }
    
    });

});