$(document).bind("mobileinit", function () {
		console.log("mobileinit");
	    $.mobile.ajaxEnabled = false;
	    $.mobile.linkBindingEnabled = false;
	    $.mobile.hashListeningEnabled = false;
	    $.mobile.pushStateEnabled = false;
	    $.mobile.defaultPageTransition = "slide";

	    // Remove page from DOM when it's being replaced
	    $('div[data-role="page"]').live('pagehide', function (event, ui) {
	    	//console.log("removing page from DOM");
	    	$(event.currentTarget).trigger('teardown');
	    	$(event.currentTarget).remove();
	    	
	    	//console.log(ui);
	    });
});