define([
	'underscore',
	'backbone',
], function (_, Backbone) {

    "use strict";

	return Backbone.Model.extend({

		//specify url endpoint
		url: function () {
			return 'data/spirits.json';
		}

	});

});