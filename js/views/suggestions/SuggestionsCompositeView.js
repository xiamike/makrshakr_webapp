define([
    // Libs
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',

    // Views
    'views/common/NavigationHeaderView',
    'views/suggestions/DrinkListView',

    // Templates
    'text!templates/suggestions/suggestionsCompositeTemplate.html',
    'models/recipe/RecipeModel',


    //Collections
    'collections/serverRecipes/ServerRecipesCollection',
    'collections/ingredients/IngredientsCollection'


], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,

    NavigationHeaderView,
    DrinkListView,

    SuggestionsCompositeTemplate,
    Recipe,

    ServerRecipesCollection,
    IngredientsCollection

) {

    "use strict";

    return Backbone.View.extend({


        initialize: function (options) {
            console.log("Init SuggestionsCompositeView");
            _.bindAll(this);

            //Assign ingredients passed from router at initialization
            //this.ingredients = this.options.ingredients;
            this.serverRecipes = new ServerRecipesCollection();
            this.ingredients = new IngredientsCollection();

        /*----- Handlebars helpers -----*/

        /*----- Listen to model changes and update navigation footer accordingly -----*/
            //this.listenTo(this.model.recipeIngredients, 'change', this.updateUI);
            //this.listenTo(this.model.recipeIngredients, 'add', this.updateUI);
            //this.listenTo(this.model.recipeIngredients, 'remove', this.updateUI);
        },


        render: function (eventName) {
            console.log("Render SuggestionsCompositeView");
            var template  = Handlebars.compile(SuggestionsCompositeTemplate);
            this.$el.html(template);
            this.loadSubViews();
            return this;
        },


    /*----- Post show setup -----*/
        //Called by router after JQM pageshow event is triggered
        //View has been loaded into DOM and transition animation is complete
        postShowSetup: function()
        {
            console.log("post show setup");
            var that = this;

            //Show loading UI
            window.EventDispatcher.trigger("showLoadingUI",
                "Loading classic recipes...");



            
            console.log("Loading ingredients and recipe suggestions");

            //Load ingredients and setup scroll menus
            this.ingredients.fetch({
                //Handle success
                success: function(data, textStatus, jqXHR)
                {
                    console.log(textStatus);
                    //Validate true success
                    if(textStatus
                        && parseInt(textStatus.code)==1
                        && textStatus.ingList.length>0)
                    {
                       

                /*****************************************/
                    
                        //Load recipe suggestions
                        that.serverRecipes.fetch({
                            //Handle success
                            success: function(data, textStatus, jqXHR)
                            {
                                //console.log(textStatus);
                                //Validate true success
                                if(textStatus
                                    && parseInt(textStatus.code)==1
                                    && textStatus.recipeList
                                    && textStatus.recipeList.length>0)
                                {
                                    console.log("Success: loaded "
                                        +textStatus.recipeList.length
                                        +" recipe suggestions");

                                    $.mobile.loading("hide"); //Hide loading UI


                                    


                                    //console.log(that.serverRecipes.models);
                                    //console.log("........");
                                    that.drinkListView = new DrinkListView({
                                        el: that.$("#drink-list-wrapper"),
                                        recipes: that.serverRecipes.models,
                                    });
                                    that.drinkListView.render();

                                } else
                                {
                                    //Dispatch error event
                                    window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                                }

                            },

                            //Handle error
                            error: function( jqXHR, textStatus, errorThrown)
                            {
                                //Dispatch error event
                                window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                            }

                        });

                /*****************************************/

                    } else
                    {
                        //Dispatch error event
                        window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                    }

                },

                //Handle error
                error: function( jqXHR, textStatus, errorThrown)
                {
                    //Dispatch error event
                    window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                }

            });





            

        },


    /*----- Load subviews -----*/
        loadSubViews: function() {
            console.log("Load SuggestionsCompositeView subviews");

            //Navigation header
            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Classic Recipes",
                nav: 'suggestions'
            });
            this.navigationHeaderView.render();

        },

    /*----- Events -----*/
        events: {
            "click      .drink"             :"tappedDrink"
        },

    /*----- Event Handlers -----*/

        tappedDrink: function (event) {

            //only allow if we have ingredients!

            //Get recipeId
            var recipeId = $(event.currentTarget).data("recipe-id");
            recipeId = parseInt(recipeId);
            recipeId = recipeId.toString()
            console.log("tapped recipe with id: "+recipeId);


            var results = this.serverRecipes.where({recipeId: recipeId});
            var serverRecipe;
            if (results.length>0)
            {
                serverRecipe = results[0];
            }
            //console.log(serverRecipe);
            
            console.log("server recipe is: ");
            console.log(serverRecipe);

            var recipe = new Recipe();
            recipe.makeFromServerRecipeAndIngredients(serverRecipe,this.ingredients);
            console.log("recipe is: ");
            console.log(recipe);
            
            window.EventDispatcher.trigger("showServerRecipe", recipe);





            event.preventDefault(); //Prevent linking
        }












    });

});