define([
    'underscore',
    'backbone',
    'models/step/Step'
], function (_, Backbone, Step) {

    "use strict";

	return Step.extend({

        initialize: function() {
            this.bind('change',function(){
                this.calculateQTY();
            });

            var type = parseInt(this.get('type'));
            var initVolume = App.initVolumeMapping[type];
            this.set('volume', initVolume);
        },

		defaults: {
            //type: 			"ingredient",
            volume: 		0.0,
            price:          0.0,
            qty:            0.0,  
        },

        getCost: function(){
            var cost = 0.0;

            var price = this.get("price");
            var volume = this.get("volume");
            price = parseFloat(price);
            volume = parseFloat(volume);
            var cost=price*volume;
            return cost;
        },

        calculateQTY: function(){
            if (this.hasChanged('volume'))
            {
                //console.log("calculating QTY");

                var tmpQty = this.get('qty');

                var volume = parseFloat(this.get('volume'));
                var type   = parseInt(this.get('type'));

                var conversionFactor = App.conversionMapping[type];
                var quantity = volume * conversionFactor;

                // TODO: Need to check individual qty.

                this.set('qty',quantity);
                this.mapUnit();
            }
          
            //console.log(this);
        },

        calculateVolume: function(){
            //console.log("calculating volume");
            var qty = this.get('qty');
            qty = parseFloat(qty);
            //console.log(qty);
            var type = parseInt(this.get('type'));
            //console.log(type);

            var ingredientId = this.get('ingredientId');
            var conversionFactor;

            if (ingredientId == 46 || ingredientId == 47 ) {
                conversionFactor = 50;
            } else {
                conversionFactor = App.conversionMapping[type];
            }


            if(conversionFactor!=0)
            {
                conversionFactor = 1/conversionFactor;
            }
            
            var volume = qty*conversionFactor;

            this.set('volume',volume);
            this.mapUnit();
        },

        mapUnit: function(){

            var type = parseInt(this.get('type'));
            if(type)
            {
                var unitName;

                var index = 0; 
                var volume = parseFloat(this.get('volume'));
                if (volume > 1) {
                    index = 1;
                }

                if (type === 5) {
                    unitName = App.unitMappingSpecial[this.get('ingredientId')][index];
                } else{
                    unitName = App.unitMapping[type][index];
                }


                this.set('units', unitName);
            }
        }

	});

});