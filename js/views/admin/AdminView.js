define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'collections/drinks/SimulatedRecipesCollection',
    'collections/users/SimulatedUsersCollection',
    'models/recipe/RecipeModel',
    'models/user/UserModel',
    'text!templates/admin/adminTemplate.html',
    'views/common/NavigationHeaderView',
    'views/home/HomeView'
], function (
    $, 
    jquerymobile, 
    _, 
    Handlebars, 
    Backbone, 
    SimulatedRecipesCollection, 
    SimulatedUsersCollection, 
    RecipeModel, 
    UserModel,
    AdminTemplate,
    NavigationHeaderView,
    HomeView
) {

    "use strict";

    return Backbone.View.extend({

        events: {
            "click .btn-order-drink"  : "orderDrink"
        },

        initialize: function (options) {

//            this.user = new UserModel();
        },

        render: function (eventName) {
            var template  = Handlebars.compile(AdminTemplate);
            this.$el.html(template); 
  //          var html = template({user:this.user.toJSON()});

            //console.log(this.user.toJSON());

            //this.$el.html(html);
            this.loadSubViews();

            // var html = template({user:this.model.toJSON()});
            // this.$el.html(html);
        },

        loadSubViews: function() {
            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Simulator"
            });
            this.navigationHeaderView.render();
        },

        orderDrink: function (event) {

            var users = new SimulatedUsersCollection();
            console.log("Calling fetch");
            var that = this;
            users.fetch({
                success: function (data) {
                    var randomIndex = Math.floor(Math.random() * (users.length));
                    var user = new UserModel();
                    user = users.at(randomIndex);

                    var recipes = new SimulatedRecipesCollection();
                    recipes.fetch({
                        success: function (data) {
                            var randomIndex = Math.floor(Math.random() * (recipes.length));
                            var recipe = new RecipeModel();
                            recipe = recipes.at(randomIndex);
                            var html = '<img src="' + user.get("userImageUrl") + '"' + 'height="100" width="100">' + '<br>';
                            html    +=  user.get('userName') + '<br>' + '<br>';
                            html    += ' ordered a ' + '<br>' + '<br>';
                            html    += recipe.get('drinkName') + '<br>';
                            html    += " recipeId:" + recipe.get('recipeId');
                            that.$("#order-added").html(html);

                            var data = {
                                email:    user.get('email'), 
                                password: user.get('password'), 
                                recipeId: recipe.get('recipeId')
                            };

                            var serializedData = JSON.stringify(data);
                            console.log(serializedData);

                            $.post("proxy/kukaproxy.php?path=placeOrder", serializedData).done(function(data){
                                console.log("placeOrder: " + data);
                            }); 
                        }
                    });
                }
            });
        }

    });

});