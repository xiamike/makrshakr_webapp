define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/design/stepsListTemplate.html',
    'models/step/IngredientStep',
    'models/step/Step',
    'models/recipe/RecipeModel',
    'text!templates/design/placeholderActionItemTemplate.html',
    'text!templates/design/actionItemTemplate.html',
    'views/design/ActionScrollMenuView',
    'text!templates/design/actionPlaceholderScrollMenuTemplate.html',
    'text!templates/design/actionScrollMenuTemplate.html',
    'text!templates/design/ingredientItemTemplate.html'

], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    StepsListTemplate,
    IngredientStep,
    Step,
    Recipe,
    PlaceholderActionItemTemplate,
    ActionItemTemplate,
    ActionScrollMenuView,
    ActionPlaceholderScrollMenuTemplate,
    ActionScrollMenuTemplate,
    IngredientItemTemplate
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function(options){
            _.bindAll(this);

            console.log("RecipeStepsListView init");

            //TODO: validate that we have a collection here

            //Assign ingredients passed from parent at initialization
            this.ingredients = options.ingredients;


        }, //end init

        render: function (eventName) {
            console.log("render RecipeStepsListView");

            //Render main template
            var template  = Handlebars.compile(StepsListTemplate);
            var steps = this.model.recipeIngredients.toJSON();
            
            var el = template({steps: steps});

            this.$el.html(el);

            var that = this;

            var totalHeight = 1; //border

            //Append each action
            $.each(this.model.recipeIngredients.models,function(index,value){
                var isIngredient = (parseInt(value.get("type")) != App.actionsIndex) ? true : false;

                var ingredientTemplate = Handlebars.compile(IngredientItemTemplate);
                var data = value.toJSON();
                
                //Human friendly index (start at 1)
                data.index = index;
                data.type = parseInt(value.get("type"));
                data.displayStepNumber = index+1;

                var ingredientEl = ingredientTemplate(data);
                
                that.$("#steps-list").append(ingredientEl);
                that.$("#steps-list").append("<li class='tickmark-container'><div class='tickmark'></div></li>");
                
                
            

            });
            
            
            //JQM create
            this.$el.trigger("create");

            var totalHeight = 1; //border
            this.$(".added-ingredient").each(function(index,element){

              //console.log(element);
              var ingredientIndex = $(element).data("step-index");
              ingredientIndex = parseInt(ingredientIndex);
              console.log(ingredientIndex);
              //Prevent access outside array
              if (ingredientIndex >= 0 && ingredientIndex < that.model.recipeIngredients.length) {
                var ingredient = that.model.recipeIngredients.at(ingredientIndex);
                var volume = ingredient.get("volume");
                var type = ingredient.get('type');

                var incrementUnit = App.initVolumeMapping[type];
    
                var height = 0 + parseFloat(volume / incrementUnit) * 44;







                //var height = 0+volume*44;
                console.log(height)
                $(element).height(height);
                totalHeight+=height+1;
              }
            });
            //this.$el.height(totalHeight);

        },

        postRenderSetup: function () {
        },

    });

});