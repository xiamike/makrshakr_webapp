define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'views/common/NavigationHeaderView',
  'views/common/NavigationFooterView',
  'views/common/RecipeGraphicView',
  'text!templates/order/orderSummaryTemplate.html',
  'views/design/WaveGraphicView',
  'views/design/ReviewListView',
  'views/design/DrinkProgressView',
  'text!templates/design/staticActionStepTemplate.html'
], function (
  $,
  jquerymobile,
  _,
  Handlebars,
  Backbone,
  NavigationHeaderView,
  NavigationFooterView,
  RecipeGraphicView,
  OrderSummaryTemplate,
  WaveGraphicView,
  ReviewListView,
  DrinkProgressView,
  StaticActionStepTemplate

  ) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {
          _.bindAll(this);
          //window.addEventListener('resize', this.resizeAndRenderWave, false);
        },

        render: function (eventName) {
          console.log("Render order summary view");
            var template  = Handlebars.compile(OrderSummaryTemplate);
            var data = {drinkName:this.model.get("name")}
            var html = template(data);
            this.$el.html(html);
            this.loadSubViews();
        },

        /*----- Wave height logic -----*/
        resizeAndRenderWave: function() {
            console.log(this.model);
            var ingredients = this.model.recipeIngredients.getIngredientsOnly();
            var numIngredients = ingredients.length;

            //Calculate height of ingredient elements
            //Make sure to account for border heights
            var totalHeight = 1;
            var that = this;
            $(ingredients).each(function(index,element){
                var volume = element.get("volume");
                var type = element.get('type');
                var incrementUnit = App.initVolumeMapping[type];

                var height = 0 + parseFloat(volume / incrementUnit) * (44)+1;

                totalHeight+=height;
            });
            var height = totalHeight;

            //Resize other elements
            //this.$("#wave-wrapper").height = height-8;

            //Hardcoding actions bar height and instructions for now!
            //var minContentHeight = 55+192+height+8;
            //console.log(this.$("#actions-bar").height());
            //console.log(this.$("#actions-bar").length);

            //this.$("#recipe-steps-actions-wrapper").css("min-height",minContentHeight);

            //Important!
            //this.$("#waveCanvas")[0].width = this.$("#wave-wrapper").width()-1;
            //console.log(this.$("#waveCanvas").width());
            //console.log(this.$("#wave-wrapper").width());


            //this.waveGraphicView.renderWave(height-8);
        },

        postShowSetup: function()
        {
            //this.resizeAndRenderWave();
            window.EventDispatcher.trigger("errorPopUp", "To order your drink, please submit pay-code which can be obtained from the MakrShakr staff!");

        },

        events:{
            "click          #nextButton"    :"clickedOrder",
            "input          #paycode"       :"enteredPaycode"
        },

        clickedOrder: function(){
            console.log("clickedOrder");
            console.log(this.model);
            window.EventDispatcher.trigger("pay",this.model);
            event.preventDefault(); //prevent linking
        },

        enteredPaycode: function(){
          var paycodeString = this.$("#paycode").attr("value");
          paycodeString = paycodeString.toUpperCase();
          console.log(paycodeString);
          this.model.set("promoCode", paycodeString);
          console.log(this.model);

        },

        loadSubViews: function() {

            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Place order /////////",
                nav: 'design'
            });
            this.navigationHeaderView.render();

            this.reviewListView = new ReviewListView({
                el: this.$("#recipe-steps-list-wrapper"),
                model: this.model
            });
            this.reviewListView.render();


            this.drinkProgressView = new DrinkProgressView({
                el: this.$("#drink-progress-wrapper"),
                model: this.model
            });
            this.drinkProgressView.render();
            this.drinkProgressView.animate();

            //WaveGraphicView
            //this.waveGraphicView = new WaveGraphicView({
            //    el: this.$("#wave-wrapper")
            //});

            //this.waveGraphicView.render();
            //this.resizeAndRenderWave();

            //Append action and strain by hand
            this.$("#recipe-actions-list-wrapper").empty();

            if(this.model.actionIngredient||this.model.strainIngredient)
            {
                this.$("#recipe-actions-list-wrapper").empty();
                this.$("#recipe-actions-list-wrapper").append("<div class='small-instructions-2'><p>Preparation method:</p></div>");
                this.$("#recipe-actions-list-wrapper").append("<ul id='action-steps-list' data-role='listview' data-inset='true' data-corners='false'></ul>");

                if(this.model.actionIngredient)
                {
                    var template  = Handlebars.compile(StaticActionStepTemplate);
                    var data = this.model.actionIngredient.toJSON();
                    var html = template(data);
                    this.$("#action-steps-list").append(html);
                }

                if(this.model.strainIngredient)
                {
                    var template  = Handlebars.compile(StaticActionStepTemplate);
                    var data = this.model.strainIngredient.toJSON();
                    var html = template(data);
                    this.$("#action-steps-list").append(html);
                }


            }



            ////////////////////////////
            /* Footer navigation here */
            ////////////////////////////

            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    false,
                nextButtonTitle:    "++ Order ++",
                nextAllowed:        true,
                nextHref:           "#order"
            });
            this.navigationFooterView.render();

            ///////////////////////////
            /* End footer navigation */
            ///////////////////////////


            //this.NextButtonView.render();
        },

    });

});