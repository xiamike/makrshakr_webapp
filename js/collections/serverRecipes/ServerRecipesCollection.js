define([
	'underscore',
	'backbone',
    'models/recipe/ServerRecipeModel',
    'collections/ingredients/IngredientsCollection'

], function (
    _,
    Backbone,
    ServerRecipeModel,
    IngredientsCollection
    ) {

	"use strict";

	return Backbone.Collection.extend({

		model: ServerRecipeModel,

        initialize: function(userId) {
            if(userId===undefined)
            {
                this.userId = 999999999;
            } else
            {
                this.userId = userId;
            }
        },

        //specify url endpoint
        url: function () {
            return App.routes.getSuggestedRecipes;
        },

        fetch: function (options) {

            options.type = 'POST';
            options.data =  JSON.stringify({'userId': this.userId });

            /*var success = function(collection, response, options){
                console.log(response);
            };

            var error = function(collection, response, options){
                console.log(response);
            };*/

            //Call Backbone's fetch
            return Backbone.Collection.prototype.fetch.call(this, options);

        },

        //specify root to models
        //TODO: additional error handling!
        parse: function(resp, xhr) {
            //return resp.ingredients;
            //console.log("parsing response");
            //console.log(resp);

            if(resp && resp.recipeList)
            {
                //console.log("yes");
                return resp.recipeList;    
            } else
            {
                return null;
            }
        }


	
	});

});