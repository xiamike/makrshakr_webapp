<?php
	$path 			= $_GET['path'];
	$baseUrl 		= 'https://www.makrshakr.com:8081/KUKAWS/';
	$url 			= $baseUrl . $path;

	$ch 			= curl_init($url);
 
	$timeout = 5;
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
	$response = curl_exec($ch);
	curl_close($ch);

	header('Content-Type: application/json');
	echo $response;
?>