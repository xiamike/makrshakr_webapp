define([
    'jquery',
    'underscore',
    'backbone',
    'collections/drinks/DrinksCollection',
    'collections/drinks/PlottableRecipesCollection',
    'models/drink/DrinkModel',
    'views/explore/ExploreVisualizationView',
    'text!templates/explore/exploreCompositeTemplate.html',
    'views/common/NavigationHeaderView',
], function (
    $,
    _,
    Backbone,
    DrinksCollection,
    PlottableRecipesCollection,
    DrinkModel,
    ExploreVisualizationView,
    ExploreCompositeTemplate,
    NavigationHeaderView
    ) {

    "use strict";

    return Backbone.View.extend({

        el: $("#page"),

        events: {
            "click #graph-age-pop"              : "graphAgePopPressed",
            "click #graph-age-alc"              : "graphAgeAlcPressed",
            "click #graph-pop-alc"              : "graphPopAlcPressed"
        },
        
        graphAgePopPressed: function (event) {
            console.log("graphAgePopPressed");
            this.exploreVisualizationView.graphAgePopularity();
            $("#y-axis-label .axis-name").text("Drink Popularity");

            $("#x-axis-label .axis-name").text("Age");
            $("#graph-title h1").text("Age vs. Drink Popularity");
        },

        graphAgeAlcPressed: function (event) {
            console.log("graphAgeAlcPressed");
            this.exploreVisualizationView.graphAgeAlcohol();
            $("#y-axis-label .axis-name").text("Alcohol %");

            $("#x-axis-label .axis-name").text("Age");
            $("#graph-title h1").text("Age vs. Alcohol %");
        },

        graphPopAlcPressed: function (event) {
            console.log("graphPopAlcPressed");
            this.exploreVisualizationView.graphPopularityAlcohol();
            $("#y-axis-label .axis-name").text("Alcohol %");

            $("#x-axis-label .axis-name").text("Drink Popularity");

            $("#graph-title h1").text("Drink Popularity vs. Alcohol %");
        },

        initialize: function(options){
            console.log("ExploreCompositeView init");

            console.log(this.options.filters);

            // this.drinks = new DrinksCollection();
            this.plottableRecipes = new PlottableRecipesCollection();

            //fetch data from resource
            $.mobile.loading("show");
        },

        render: function (eventName) {
            console.log("render ExploreCompositeView");
            var template  = Handlebars.compile(ExploreCompositeTemplate);
            this.$el.html(template);

            this.loadSubViews();
            $("#page").trigger("pagecreate");
            //return this;
        },

        postShowSetup: function() {
            $("#y-axis-label .axis-name").text("Drink Popularity");

            $("#x-axis-label .axis-name").text("Age");
            $("#graph-title h1").text("Age vs. Drink Popularity");
            console.log("HI");
        },


        loadSubViews: function() {
            console.log("Loading subviews");

            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Explore",
                nav: 'explore'

            });

            this.navigationHeaderView.render();

            //console.log(this.$("#visualization-wrapper"));
            this.exploreVisualizationView = new ExploreVisualizationView({
                el: this.$("#visualization-wrapper"),
                collection:  this.plottableRecipes
            });

            this.exploreVisualizationView.render();
            console.log("*******");
            console.log($("#visualization-wrapper").length);

            $("#y-axis-label .axis-name").text("Drink Popularity");

            $("#x-axis-label .axis-name").text("Age");
            $("#graph-title h1").text("Age vs. Drink Popularity");
            console.log("HI");

            var that = this;
            console.log("Loading drinks . . .");

            this.plottableRecipes.fetch({
                //TODO: handle failure
                success: function(data)
                {
                    console.log("Success loading drinks");

                    console.log(that.plottableRecipes.toJSON());

                    $.mobile.loading("hide");

                    var plottableRecipes = that.plottableRecipes.toJSON();

                    that.exploreVisualizationView.renderVisualization(plottableRecipes);
                }
            });
            

        },

    });

});
