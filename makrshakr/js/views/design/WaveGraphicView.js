define([
    'jquery',
    'underscore',
    'backbone',
    'paper',
    'd3',
], function (
    $,
    _,
    Backbone,
    Paper,
    D3
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function(){
            console.log("Init WaveGraphicView");
        },

        render: function (eventName) {
            console.log("Render WaveGraphicView");
        },



        renderWave: function (height){

            // FIXME: Stop showing wave.
            return false;

            console.log("render wave");
            var canvas = this.$("#waveCanvas")[0];
            //console.log("canvas is:");
            //console.log(canvas);
            paper.setup(canvas);
            var canvasHeight = height;
            canvas.height = canvasHeight;
            this.height = canvasHeight;
            paper.view.size.height = canvasHeight;
            

           // The amount of segment points we want to create:
            var amount = 5;

            // The maximum height of the wave:
            var height = 10;

            // Create a new path and style it:
            var path = new paper.Path();

            path.style = {
                strokeColor: '#0081FF',
                strokeWidth: 0,
                fillColor: '#0081FF'
            };


            // Add segment points to the path spread out
            // over the width of the view:
            var bottomLeftPoint = new paper.Point(-1/amount*paper.view.size.width, paper.view.size.height);
            var bottomRightPoint = new paper.Point(paper.view.size.width*(amount+1)/amount, paper.view.size.height);
            //console.log(bottomLeftPoint.x+","+bottomLeftPoint.y);
            path.add(bottomLeftPoint);
            for (var i = 0; i <= amount+2; i++) {
                var point = new paper.Point((i-1)/ amount, 1);
                point.x = point.x * paper.view.size.width;
                point.y = point.y * paper.view.size.height/2;
                path.add(point);
                //console.log(point.x+","+point.y);
            }
            path.add(bottomRightPoint);
            path.closePath();
            //console.log(bottomRightPoint.x+","+bottomRightPoint.y);
            //console.log(path.segments.length);
            // Select the path, so we can see how it is constructed:
            //path.selected = true;
            var that = this;
            paper.view.onResize = function(event) {
                //console.log("resize");
                canvas.height = that.height;
                paper.view.size.height = that.height;

                paper.view.size.width = paper.view.size.width;
                path.removeSegments();

                path.style = {
                    strokeColor: '#0081FF',
                    strokeWidth: 0,
                    fillColor: '#0081FF'
                };


                // Add 5 segment points to the path spread out
                // over the width of the view:
                var bottomLeftPoint = new paper.Point(-1/amount*paper.view.size.width, paper.view.size.height);
                var bottomRightPoint = new paper.Point(paper.view.size.width*(amount+1)/amount, paper.view.size.height);
                //console.log(bottomLeftPoint.x+","+bottomLeftPoint.y);
                path.add(bottomLeftPoint);
                for (var i = 0; i <= amount+2; i++) {
                    var point = new paper.Point((i-1)/ amount, 1);
                    point.x = point.x * paper.view.size.width;
                    point.y = point.y * paper.view.size.height/2;
                    path.add(point);
                    //console.log(point.x+","+point.y);
                }
                path.add(bottomRightPoint);
                path.closePath();
                //path.selected = true;

                
            }

            paper.view.onFrame = function(event) {
                // Loop through the segments of the path:
                for (var i = 0; i <= amount+4; i++) {
                    var segment = path.segments[i];

                    //TODO: or use array position
                    //console.log(segment.point.y);
                    //console.log(paper.view.size.height);
                    if(segment.point.y<paper.view.size.height)
                    {
                        

                        // A cylic value between -1 and 1
                        var sinus = Math.sin(event.time*3 + i);
                        
                        // Change the y position of the segment point:
                        segment.point.y = sinus * height + height+6;
                        //console.log(sinus);
                        //console.log(segment.point.y);
                    }
                }
                // Uncomment the following line and run the script again
                // to smooth the path:
                path.smooth();
            }

        },

    });

});
