define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'text!templates/login/loginEmailTemplate.html',
  'views/common/NavigationFooterView',
  'models/user/UserModel'
], function(
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    LoginEmailTemplate,
    NavigationFooterView,
    User
    ) {

    "use strict";

    return Backbone.View.extend({

        events:{
            "click          #nextButton"     :"nextButtonPressed"
        },

        initialize: function (options) {
            _.bindAll(this);

            this.newUserObject = new User();
        },

        render: function (eventName) {
            var template  = Handlebars.compile(LoginEmailTemplate);
            this.$el.html(template);
            this.$el.addClass('login-content-wrapper');
            this.loadSubViews();
        },

        loadSubViews: function() {
            console.log("LoginEmailView >> load subviews");

            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    true,
                nextButtonTitle:    this.nextButtonTitle,
                nextAllowed:        true
            });
            this.navigationFooterView.render();
        },


        nextButtonPressed: function (event) {
            //Set email
            var email = this.$("#email").attr("value").toLowerCase();
            this.newUserObject.set("email", email);

            //Set password
            var password = this.$("#password").attr("value").toLowerCase();
            this.newUserObject.set("password", password);

            var that = this;

            window.EventDispatcher.trigger("showLoadingUI", "Logging in...");

            var serializedUserData = JSON.stringify(this.newUserObject);

            console.log(">>>>>>>>>>>>>>>>>>>>");
            console.log("Attempting email login");
             $.post("proxy/kukaproxy.php?path=login",
                serializedUserData)

             //Success handler
             .done( function(data, textStatus, jqXHR)
             {
                console.log("==========");
                console.log("Login user response: ");
                console.log(data);
                console.log(textStatus);
                console.log("==========");

                /*IMPORTANT*/
                //On register, success returned in data.success = 1
                //On login, success is returned in data.code = 1
                var successCode = data.code;
                successCode = parseInt(successCode,10);

                if (successCode && successCode > 0) {
                    console.log("Login success");

                    //Dispatch loginSuccess
                    //Sending the user object we created locally
                    //And the server response
                    window.EventDispatcher.trigger(
                        "loginSuccess",
                        that.newUserObject,
                        data);

                
                //Server response code is failure
                //Show failure message
                } else
                {
                    var message = data.message;
                    window.EventDispatcher.trigger("errorPopUp", "Login failed: " +data.message);
                    console.log("==========");
                    console.log("Login user response: ");
                    console.log(data);
                    console.log(textStatus);
                    console.log(data);
                    console.log("==========");
                }

                
            }) //End success handler
            
            //Failure callback
            .fail(function(jqXHR, textStatus, errorThrown)
            {
                console.log("Failed to login");
                window.EventDispatcher.trigger("errorPopUp", "Server error: " +errorThrown);
            });



            event.preventDefault();
        }

    });

});