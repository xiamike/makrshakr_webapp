define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'text!templates/login/createSelectTemplate.html',
], function(
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    CreateSelectTemplate
    ) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {
            _.bindAll(this);

        },

        render: function (eventName) {
            var template  = Handlebars.compile(CreateSelectTemplate);

            this.$el.addClass('login-content-wrapper');
            this.$el.html(template);
            this.loadSubViews();
        },

        loadSubViews: function() {

        }

    });

});