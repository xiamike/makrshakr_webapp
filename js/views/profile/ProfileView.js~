define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'views/common/NavigationFooterView',
  'views/common/NavigationHeaderView',
  'text!templates/profile/profileTemplate.html',
  'views/suggestions/DrinkListView',
  'collections/ingredients/IngredientsCollection',
  'collections/serverRecipes/ServerRecipesCollection',
  'models/recipe/RecipeModel'

], function(
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    NavigationFooterView,
    NavigationHeaderView,
    ProfileTemplate,
    DrinkListView,
    IngredientsCollection,
    ServerRecipesCollection,
    Recipe
    
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {
            _.bindAll(this);
            this.message = this.options.message;

            //this.serverRecipes = new ServerRecipesCollection();
            this.ingredients = new IngredientsCollection();
        },

        render: function (eventName) {

            var template  = Handlebars.compile(ProfileTemplate);

            var data = {
                user: this.model.toJSON(),
                message: this.message
            };

            var html = template(data);
            this.$el.html(html);

            this.$("#profile-hexagon").empty();

            var drawContainer = d3.select(this.$("#profile-hexagon")[0]);

            var width =         120;
            var height =        120;
            var hexRadius =     52;

            var svgContainer = drawContainer.append("svg")
                .attr("width",width)
                .attr("height",height);

            //Helper function for constructing lines
            var lineFunction = d3.svg.line()
                .x(function(d) { return d.x; })
                .y(function(d) { return d.y; })
                .interpolate("linear");


            //Set clipping path
            var defs = svgContainer.append('svg:defs');
            var that = this;
            /*
            var hexagonClipPath = defs.append('clipPath')
                .attr('id','hex-clip-path')
                .append("path")
                .attr("d", function(d){
                    var x = width/2;
                    var y = width/2;
                    var points = that.hexagonPoints(x,y,hexRadius);
                    return lineFunction(points)+"Z";
                });
*/


           var baseLayer = svgContainer.append("image")
                    .attr('xlink:href', this.model.get("userImageUrl"))
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  width)
                    .attr('height', height);

//                    .attr("clip-path","url(#hex-clip-path)");


            var profileHexgon;
            if (this.model.get('gender') == 'female') {
                profileHexgon = "images/hexagon-profile-female-fill.png";
            } else {
                profileHexgon = "images/hexagon-profile-male-fill.png";
            }

            var overlay = svgContainer.append("image")
                    .attr('xlink:href', profileHexgon)
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  width)
                    .attr('height', height);

            var userName = this.model.get("userName");
            var age = this.model.get("age");
            var nationality = this.model.get("nationality");
            var gender = this.model.get("gender");

            //this.$("#profile-bio").empty();
            this.$("#profile-bio").html(
                "<p class='user-name'>"+userName+"</p>"
                +
                "<p class='age-gender'>"+age+", "+gender+"</p>"
                +
                "<p class='nationality'>"+nationality+"</p>"
            );

            this.loadSubviews();
        },


            /*----- Post show setup -----*/
        //Called by router after JQM pageshow event is triggered
        //View has been loaded into DOM and transition animation is complete
        //iScroll requires elements to be loaded and visible to calculate dimensions
        postShowSetup: function()
        {
            console.log("Post show setup");
            var that = this;

            //Load user's recipes
            


            var userId = this.model.get("userId");
            userId = parseInt(userId,10);
            console.log(userId);

            if(!userId || typeof(userId)!="number")
            {
                return false;
            }

            window.EventDispatcher.trigger("showLoadingUI", "Loading your recipes...");
            var requestData = {
                recipeId: -1,
                userId: userId
            };
            this.serverRecipes = new ServerRecipesCollection(userId);
            
            //Load ingredients and recipes
            /*!!!!!!!!!!!!!!!!!!!!!!!!!!*/
            this.ingredients.fetch({
                //Handle success
                success: function(data, textStatus, jqXHR)
                {
                    console.log(textStatus);
                    //Validate true success
                    if(textStatus
                        && parseInt(textStatus.code)==1
                        && textStatus.ingList.length>0)
                    {
                       

                /*****************************************/
                    
                        //Load recipe suggestions
                        that.serverRecipes.fetch({
                            //Handle success
                            success: function(data, textStatus, jqXHR)
                            {
                                //console.log(textStatus);
                                //Validate true success
                                // nlam: Hack with the success code
                                console.log('>>>>>>>>>>>>>>');
                                console.log(textStatus);
                                console.log(parseInt(textStatus.code));
                                console.log(textStatus.recipeList);
                                // console.log(textStatus.recipeList.length);
                                console.log('>>>>>>>>>>>>>>');

                                //  FIXME: more hacks
                                // if(textStatus
                                //     && parseInt(textStatus.code)==1 
                                //     && textStatus.recipeList
                                //     && textStatus.recipeList.length>0)

                                if(textStatus
                                    && parseInt(textStatus.code)==1 
                                    && textStatus.recipeList)
                                {
                                    console.log("Success: loaded "
                                        +textStatus.recipeList.length
                                        +" recipe suggestions");

                                    $.mobile.loading("hide"); //Hide loading UI


                                    //console.log(that.serverRecipes.models);
                                    //console.log("........");
                                    that.drinkListView = new DrinkListView({
                                        el: that.$("#drink-list-wrapper"),
                                        recipes: that.serverRecipes.models,
                                    });
                                    that.drinkListView.render();
                                    that.$(".message-header-text").text("My recipes");
                                } 
                                // FIXME: Hack to make this work, Mike needs to update the backend.
                                else if (parseInt(textStatus.code)==-1)  {
                                    $.mobile.loading("hide"); //Hide loading UI
                                }
                                else
                                {
                                    //Dispatch error event
                                    window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                                }

                            },

                            //Handle error
                            error: function( jqXHR, textStatus, errorThrown)
                            {
                                //Dispatch error event
                                window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                            }

                        });

                /*****************************************/

                    } else
                    {
                        //Dispatch error event
                        window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                    }

                },

                //Handle error
                error: function( jqXHR, textStatus, errorThrown)
                {
                    //Dispatch error event
                    window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                }

            });
            /*!!!!!!!!!!!!!!!!!!!!!!!!!!*/







        },





        /*----- Events -----*/
        events: {
            "click      .drink"             :"tappedDrink"
        },

        /*----- Event Handlers -----*/

        tappedDrink: function (event) {

            //only allow if we have ingredients!

            //Get recipeId
            var recipeId = $(event.currentTarget).data("recipe-id");
            recipeId = parseInt(recipeId);
            recipeId = recipeId.toString()
            console.log("tapped recipe with id: "+recipeId);


            var results = this.serverRecipes.where({recipeId: recipeId});
            var serverRecipe;
            if (results.length>0)
            {
                serverRecipe = results[0];
            }
            //console.log(serverRecipe);
            
            console.log("server recipe is: ");
            console.log(serverRecipe);

            var recipe = new Recipe();
            recipe.makeFromServerRecipeAndIngredients(serverRecipe,this.ingredients);
            console.log("recipe is: ");
            console.log(recipe);
            
            window.EventDispatcher.trigger("showServerRecipe", recipe);
            window.EventDispatcher.trigger("showServerRecipeFromProfile", recipe);




            event.preventDefault(); //Prevent linking
        },








        //Generate array of hexagon points given center X and Y and a radius
        hexagonPoints: function(centerX, centerY, radius){
            var angleStep = 2*Math.PI/6;
            var points = [];
            for(var i = 0; i<6; i++)
            {
                var x = centerX + Math.cos(angleStep*i+angleStep/2)*radius;
                var y = centerY + Math.sin(angleStep*i+angleStep/2)*radius;
                var point = [];
                point["x"] = x;
                point["y"] = y;
                points[i]=point;
            }
            return points;
        },

        loadSubviews: function () {
            //Navigation header
            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Profile",
                nav: "profile"
            });
            this.navigationHeaderView.render();



            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    false,
                nextButtonTitle:    "+++++ Design a drink +++++",
                nextAllowed:        true,
                nextHref:           "#ingredients"
            });
            this.navigationFooterView.render();
        }


    });

});