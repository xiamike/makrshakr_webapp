define([
    'jquery',
    'underscore',
    'backbone',
    'models/drink/DrinkModel'
], function ($, _, Backbone, DrinkModel) {

    "use strict";

    return Backbone.Collection.extend({

        //specify model
        model: DrinkModel,

        //specify url endpoint
        url: function () {
            return 'data/drinks.json';
        },

        //specify root to models
        parse: function(resp, xhr) {
            return resp.drinks;
        }

        /*getCategory: function(categoryName){
            var results = this.where({type:categoryName});
            var json = _.map( results, function( model ){ return model.toJSON() });
            return json;
        },

        getIngredient:function(ingredientID){
            var results = this.where({id:ingredientID});
            if(results.length>0)
            {
                return results[0];
            } else
            {
                return null;
            }
        }*/
    
    });

});