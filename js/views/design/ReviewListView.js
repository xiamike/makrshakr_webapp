define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/design/stepsListTemplate.html',
    'models/step/IngredientStep',

    'models/step/Step',
    'models/recipe/RecipeModel',
    'text!templates/design/placeholderActionItemTemplate.html',
    'text!templates/design/actionItemTemplate.html',
    'views/design/ActionScrollMenuView',
    'text!templates/design/actionPlaceholderScrollMenuTemplate.html',
    'text!templates/design/actionScrollMenuTemplate.html',
    'text!templates/design/staticIngredientItemTemplate.html',
    'text!templates/design/reviewListTemplate.html'

], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    StepsListTemplate,
    IngredientStep,

    Step,
    Recipe,
    PlaceholderActionItemTemplate,
    ActionItemTemplate,
    ActionScrollMenuView,
    ActionPlaceholderScrollMenuTemplate,
    ActionScrollMenuTemplate,
    StaticIngredientItemTemplate,
    ReviewListTemplate
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function(options){
            _.bindAll(this);
            console.log("ReviewListView init");

        },

        render: function (eventName) {
            console.log("render ReviewListView");
            console.log(this.model);
            //Render main template
            var template  = Handlebars.compile(ReviewListTemplate);
            var steps = this.model.recipeIngredients.toJSON();
            
            var el = template({steps: steps});

            this.$el.html(el);

            var that = this;

            var totalHeight = 1; //border

            //Append each action
            $.each(this.model.recipeIngredients.models,function(index,value){
                //console.log("actions index is:");
                //console.log(App.actionsIndex);
                //console.log("ingredient type is: ");
                //console.log(value.get("type"));

                //console.log("ingredient name is: ");
                //console.log(value.get("ingredientName"));
                var isIngredient = (parseInt(value.get("type")) != App.actionsIndex) ? true : false;

                if(isIngredient)
                {
                    var ingredientTemplate = Handlebars.compile(StaticIngredientItemTemplate);
                    var data = value.toJSON();
                    
                    //Human friendly index (start at 1)
                    data.index = index;
                    data.displayStepNumber = index+1;
                    data.type = parseInt(value.get("type"));
                    data.volume = data.volume.toFixed(2);

                    var ingredientEl = ingredientTemplate(data);
                    
                    that.$("#steps-list").append(ingredientEl);
                    that.$("#steps-list").append("<li class='tickmark-container'><div class='tickmark'></div></li>");
                }
                

            });
            
            
            //JQM create
            this.$el.trigger("create");

            var totalHeight = 1; //border
            this.$(".added-ingredient").each(function(index,element){

              //console.log(element);
              var ingredientIndex = $(element).data("step-index");
              ingredientIndex = parseInt(ingredientIndex);
              //console.log(ingredientIndex);
              //Prevent access outside array
              if (ingredientIndex >= 0 && ingredientIndex < that.model.recipeIngredients.length) {
                var ingredient = that.model.recipeIngredients.at(ingredientIndex);
                var volume = ingredient.get("volume");
                var type = ingredient.get('type');

                var incrementUnit = App.initVolumeMapping[type];
    
                // nlam
                var height = 0 + parseFloat(volume / incrementUnit) * 44;
                if(height<44)
                {
                    height = 44;
                }
                // height = parseInt(height / 44) * 44;


                //var height = 0+volume*44;
                console.log(height)
                $(element).height(height);
                totalHeight+=height+1;
              }
            });
            //this.$el.height(totalHeight);

        },

        postRenderSetup: function () {
        },

    });

});