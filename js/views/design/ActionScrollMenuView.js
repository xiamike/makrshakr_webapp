define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/design/actionScrollMenuTemplate.html'
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    ActionScrollMenuTemplate
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function() {
            this.render();
        },

        render: function () {
            var el = Handlebars.compile(ActionScrollMenuTemplate),
            
            scrollViewHTML = el(this.options.data);

            this.$el.html(scrollViewHTML);

            this.resizeScroller();
            this.initScroller();
        },

        resizeScroller: function() {
            var scrollEl = this.$(".action-scroll-view ul li");
            var numItems = this.options.data.items.length;
            var width = 222; //Hardcoding
            var margin = 0;
            //var width = parseInt(scrollEl.css("width"));
            //var margin = parseInt(scrollEl.css("margin-left"));
            var totalWidth = numItems * width + (numItems+1) * margin;

            this.$(".action-scroll-view").width(totalWidth);
            this.$(".action-scroll-view ul").width(totalWidth);
        },

        initScroller: function() {
            var s = this.$(".action-scroll-wrapper")[0];
            this.scrollView = new iScroll(s, {
                hScrollbar      : false, 
                vScrollbar      : false, 
                fixedScrollbar  : true
            });

            //use vmousemove for testing (captures both mouse and touch moves)
            $(document).bind('vmousemove', function (e) {
                //console.log(e.target.id);
                if (e.target.id == "types-scroll-wrapper") {
                    e.preventDefault();
                }
            });

            var that = this;
            setTimeout(function () { that.scrollView.refresh(); }, 0);

        }

    });

});