define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'text!templates/suggestions/drinkListTemplate.html',
  'text!templates/suggestions/drinkListItemTemplate.html',
  'views/suggestions/DrinkListItemView',
  'models/recipe/RecipeModel'
], function (
  $,
  jquerymobile,
  _,
  Handlebars,
  Backbone,
  DrinkListTemplate,
  DrinkListItemTemplate,
  DrinkListItemView,
  Recipe
) {

    "use strict";

    return Backbone.View.extend({


        initialize: function (options) {
            //console.log("DrinkListView init");
            _.bindAll(this);
            this.recipes = this.options.recipes;
            
        },

        render: function () {
            var template  = Handlebars.compile(DrinkListTemplate);

            var el = template();
            this.$el.html(el);

            var that = this;
            //console.log(this.recipes);
            $.each(this.recipes,function(index,value){

              var itemView = new DrinkListItemView({
                //el: that.$("#drinks-list"),
                model: value,
              });
              var itemView = itemView.render();
              that.$("#drinks-list").append(itemView.el);
            });

            this.$el.trigger("create");
        },

        postRenderSetup: function () {
        }

    });

});