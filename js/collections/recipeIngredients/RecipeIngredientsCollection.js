define([
    'underscore',
    'backbone',
    'models/step/Step'
], function (_, Backbone, Step) {

     "use strict";

    return Backbone.Collection.extend({

        model: Step,

        //Get cost of recipe
        //TODO: Refactor (not working atm)
        getCost: function(){
            var cost = 0.0;
            $.each(this.models,function(index,step){
                
                var price = step.get("price");
                price = parseFloat(price);

                var volume = step.get("volume");
                volume = parseFloat(volume);

                cost+=price*volume;
            });
            return cost;
        },

        getIngredientsOnly: function(){
            var results = [];
            $.each(this.models,function(index,ingredient){
                
                var type = ingredient.get("type");
                type = parseInt(type,10);
                if(type!=App.actionsIndex)
                {
                    results.push(ingredient);
                }
            });
            return results;
        },

        getTotalUnits: function(){

        },

        doesContainIngredient: function(ingredientID){
            console.log("ingredient id is");
            console.log(ingredientID.toString());
            var results = this.where({ingredientId:ingredientID.toString()});
            
            if(results.length>0)
            {
                return true;
            } else
            {
                return false;
            }
        }


    
    });

});