define([
    'jquery',
    'underscore',
    'backbone',
    'd3',
    'text!templates/common/recipeGraphicTemplate.html'
], function ($, _, Backbone, D3, RecipeGraphicTemplate) {

    "use strict";

    return Backbone.View.extend({
        el: "#recipe-graphic-wrapper",

        initialize: function(){
            //console.log("init RecipeGraphicView");
        },

        render: function (eventName) {
            //console.log("render RecipeGraphicView");

            var template  = Handlebars.compile(RecipeGraphicTemplate);
            var el = template({recipe: null});
            this.$el.append(el);
        },

        //Generate array of hexagon points given center X and Y and a radius
        hexagonPoints: function(centerX, centerY, radius){
            var angleStep = 2*Math.PI/6;
            var points = [];
            for(var i = 0; i<6; i++)
            {
                var x = centerX + Math.cos(angleStep*i+angleStep/2)*radius;
                var y = centerY + Math.sin(angleStep*i+angleStep/2)*radius;
                var point = [];
                point["x"] = x;
                point["y"] = y;
                points[i]=point;
            }
            return points;
        },

        //D3 line helper function
        d3Line: function(){
             d3.svg.line()
                .x(function(d){
                    return d.x;
                })
                .y(function(d){
                    return d.y;
                })
                .interpolate("linear");
        },

    /*----- Start Render Graphic -----*/
        renderGraphic: function (w,h,r,showText){
            //console.log("Rendering visualization");

        /*----- Prepare for drawing -----*/

            //Clear drawing container
            this.$("#recipe-graphic-canvas").empty();

            //Select drawing container with d3 (can't select Jquery object)
            var drawContainer = d3.select(this.$("#recipe-graphic-canvas")[0]);

            //Set dimensions
            //var width =         400;
            //var height =        200;
            //var hexRadius =     70;
            //var centerY =       100;
            var width =         w;
            var height =        h;
            var hexRadius =     r;
            var centerY =       h/2;
            var centerX;
            if(showText==false)
            {
                centerX = width/2;
            } else
            {
                centerX = hexRadius;
            }

        /*----- SVG element for drawing -----*/
            var svgContainer = drawContainer.append("svg")
                .attr("width",width)
                .attr("height",height);

            //Helper function for constructing lines
            var lineFunction = d3.svg.line()
                .x(function(d) { return d.x; })
                .y(function(d) { return d.y; })
                .interpolate("linear");


        /*----- Clipping path -----*/
            var defs = svgContainer.append('svg:defs');
            var that = this;
            var hexagonClipPath = defs.append('clipPath')
                .attr('id','hex-clip-path')
                .append("path")
                .attr("d", function(d){
                    var x = centerX;
                    var y = centerY;
                    var points = that.hexagonPoints(x,y,hexRadius);
                    return lineFunction(points)+"Z";
                });


        /*----- Patterns -----*/
            var pattern1 = defs.append('pattern')
                .attr('id','stripe-1-pattern')
                .attr('patternUnits', 'userSpaceOnUse')
                .attr('x',0)
                .attr('y',0)
                .attr('width',  5)
                .attr('height', 5)
                .append('image')
                    .attr('xlink:href','images/grey-stripe-pattern.png')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  5)
                    .attr('height', 5)

            var pattern2 = defs.append('pattern')
                .attr('id','stripe-2-pattern')
                .attr('patternUnits', 'userSpaceOnUse')
                .attr('x',0)
                .attr('y',0)
                .attr('width',  5)
                .attr('height', 5)
                .append('image')
                    .attr('xlink:href','images/red-stripe-pattern.png')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  5)
                    .attr('height', 5)

            var pattern3 = defs.append('pattern')
                .attr('id','stripe-3-pattern')
                .attr('patternUnits', 'userSpaceOnUse')
                .attr('x',0)
                .attr('y',0)
                .attr('width',  5)
                .attr('height', 5)
                .append('image')
                    .attr('xlink:href','images/blue-stripe-pattern.png')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  5)
                    .attr('height', 5)

            var pattern4 = defs.append('pattern')
                .attr('id','stripe-4-pattern')
                .attr('patternUnits', 'userSpaceOnUse')
                .attr('x',0)
                .attr('y',0)
                .attr('width',  5)
                .attr('height', 5)
                .append('image')
                    .attr('xlink:href','images/orange-stripe-pattern.png')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  5)
                    .attr('height', 5)

            var pattern5 = defs.append('pattern')
                .attr('id','stripe-5-pattern')
                .attr('patternUnits', 'userSpaceOnUse')
                .attr('x',0)
                .attr('y',0)
                .attr('width',  5)
                .attr('height', 5)
                .append('image')
                    .attr('xlink:href','images/green-stripe-pattern.png')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  5)
                    .attr('height', 5)

            var pattern6 = defs.append('pattern')
                .attr('id','stripe-6-pattern')
                .attr('patternUnits', 'userSpaceOnUse')
                .attr('x',0)
                .attr('y',0)
                .attr('width',  5)
                .attr('height', 5)
                .append('image')
                    .attr('xlink:href','images/blue-stripe-pattern.png')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  5)
                    .attr('height', 5)




        var ingredients = this.model.recipeIngredients.getIngredientsOnly();
        var ingredientsJson = JSON.stringify(ingredients);
        var numIngredients = ingredients.length;

        /*----- Base layer -----*/
        /*
            var baseLayer = svgContainer.append("rect")
                .attr("width", width)
                .attr("height",height)
                .attr("x",0)
                .attr("y",0)
                .style("fill","white")
                .attr("clip-path","url(#hex-clip-path)");*/

        if(showText)
        {
        /*----- Text -----*/
            var text = svgContainer.selectAll("ingredient-labels")
                .data(ingredients)
                .enter().append("text")
                .text(function(d){
                    //console.log(d);
                    return d.get("ingredientName");
                })
                .style("font-family","ThreeSix11Pro-Reg")
                .style("font-size","12px")
                .style("fill","black")
                .attr("x",hexRadius*2+12)
                .attr("y",function(d,i){
                    var barHeight = hexRadius*2/numIngredients;
                    return barHeight/2 + barHeight * (i) + (height/2-hexRadius)+ (centerY-height/2)+3;
                })

        /*----- Arrow lines -----*/
            var arrows = svgContainer.selectAll("arrow-lines")
                .data(ingredients)
                .enter().append("line")
                .attr("x1",hexRadius+ (height/2-hexRadius))
                .attr("y1",function(d,i){
                    var barHeight = hexRadius*2/numIngredients;
                    return barHeight/2 + barHeight * (i) + (height/2-hexRadius) + (centerY-height/2);
                })
                .attr("x2",hexRadius*2+8)
                .attr("y2",function(d,i){
                    var barHeight = hexRadius*2/numIngredients;
                    return barHeight/2 + barHeight * (i) + (height/2-hexRadius) + (centerY-height/2);
                })
                .style("stroke","#000")
                .style("stroke-width", "1")
                .style("stroke-opacity",1.0);
        }


        /*----- Ingredient Bars -----*/


            var ingredientBars = svgContainer.selectAll("bars")
            .data(ingredients)
            .enter().append("rect")
            .attr("class","ingredient-bar")
            .attr("width",width)
            .attr("height",hexRadius*2/numIngredients)
            .attr("x",0)
            .attr("y",function(d,i){
                //console.log("index is");
                //console.log(i);
                var barHeight = hexRadius*2/numIngredients;
                return barHeight * i + (height/2-hexRadius) + (centerY-height/2);
            })
            .style("fill-opacity",1.0)
            .style("fill",function(d,i){
                //console.log(d);
                //console.log("^^^^^^^^");
                var type = d.get("type");
                type = parseInt(type);
                
                if (type > 6)
                {
                    type = 3;
                }

                //type = Math.floor(Math.random()*5+1);

                return "url('#stripe-"+type+"-pattern')"
            })
            .style("stroke-opacity",1)
            .style("stroke-width",1)
            .style("stroke","white")
            .attr("clip-path","url(#hex-clip-path)");


        /*----- Outline path -----*/
            var hexagon = svgContainer.append("path")
                .attr("class","hexagon")
                .attr("d", function(d){
                    var x = centerX;
                    var y = centerY;
                    var points = that.hexagonPoints(x,y,hexRadius);
                    return lineFunction(points)+"Z";
                })
                .style("fill-opacity",0.0)
                .style("stroke-opacity",1.0)
                .style("stroke-width",4)
                .style("stroke","white")

            //Custom outline for Google Moonshot
            if(this.model.get('drinkName') == 'Moonshot') {
                hexagon.style("stroke", '#f28d00');
            }

        },
    /*----- End Render Graphic -----*/

    });

});
