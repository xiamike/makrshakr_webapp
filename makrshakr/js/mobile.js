require.config({
	paths: {
		jquery: 		'libs/jquery/jquery',
		jquerymobile: 	'libs/jquery.mobile/jquery.mobile',
		jqmconfig: 		'libs/jquery.mobile/jqm-config',
		iscroll: 		'libs/iScroll/iscroll',
		d3:             'libs/d3/d3.v3.min',
		paper: 			'libs/paperjs/paper',  
		handlebars: 	'libs/handlebars/handlebars',
		underscore: 	'libs/underscore/underscore',
    scrollTo:   'libs/scrollTo/jquery.scrollTo-1.4.5.min', 
		backbone: 		'libs/backbone/backbone',
		templates: 		'../templates'
	},
	 // Sets the configuration for your third party scripts that are not AMD compatible
      shim: {

            'backbone': {
                  'deps': [ 'underscore', 'jquery' ],
                  'exports': 'Backbone'  //attaches "Backbone" to the window object
            },

            'handlebars': {
            	'exports': 'Handlebars' //attaches "Handlebars" to the window object
            },

            'jqmconfig': {
            	'deps': [ 'jquery'],
            	'exports': 'JQMConfig'
            },

            'iscroll': {
            },

            'd3': {

            },

            'paper': {

            },

            'scrollTo': {
              'deps': ['jquery']
            }

      } // end Shim Configuration

});

// Includes File Dependencies
require([ "jquery", "backbone", "router", "jqmconfig", "iscroll", "d3", "paper", "scrollTo"], function($, Backbone, Router, JQMConfig, iScroll, D3, paper, scrollTo) {
	$(document).ready(function () {
    	//console.log('document ready');
	});

	require( [ "jquerymobile"], function() {
		//console.log("requiring jquerymobile");
		// Instantiates a new Backbone.js Mobile Router
		this.router = new Router();
		Backbone.history.start();
	});
});


// Use if we store application separate from bootstrap
/*
require([
	'app',
], function(App){
	App.initialize();
});*/