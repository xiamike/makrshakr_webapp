define([
    'jquery',
    'underscore',
    'backbone',
    'models/ingredients/IngredientsModel'
], function ($, _, Backbone, IngredientsModel) {

     "use strict";

    return Backbone.Collection.extend({

        //specify model
        model: IngredientsModel,

        //specify url endpoint
        url: function () {
            //return 'data/ingredients.json';
            return App.routes.getAllIngredients;
        },

        //specify root to models
        //TODO: additional error handling!
        parse: function(resp, xhr) {
            //return resp.ingredients;
            if(resp&&resp.ingList)
            {
                return resp.ingList;    
            } else
            {
                return null;
            }
        },

        /*getCategory: function(categoryName){
            var results = this.where({type:categoryName});
            var json = _.map( results, function( model ){ return model.toJSON() });
            return json;
        },*/

        getType: function(typeId){
            var results = this.where({type:typeId.toString()});
            var json = _.map(
                results,
                function( model ){
                    return model.toJSON();
                });

            return json;
        },

        getIngredient: function(ingredientID){
            console.log("searching for ingredient id:");
            console.log(ingredientID.toString());
            var results = this.where({ingredientId:ingredientID.toString()});
            if(results.length>0)
            {
                return results[0];
            } else
            {
                return null;
            }
        },

        getAllActions: function(){
            
            var results = this.where({type: App.actionsIndex.toString() });


            if(results.length>0)
            {
                var json = _.map(
                    results,
                    function( model ){
                        return model.toJSON();
                    });

                return json;
            } else
            {
                return null;
            }
        },

        getAllActionsWithoutStrain: function(){
            
            var results = this.where({type: App.actionsIndex.toString() });
            var actions = results.slice(); //copy

            $(results).each(function(index,item){
                var ingredientId = item.get("ingredientId");
                ingredientId = parseInt(ingredientId,10);
                if(ingredientId==App.strainId)
                {
                    actions.splice(index,1);
                    return;
                }
            });

            if(actions.length>0)
            {
                var json = _.map(
                    actions,
                    function( model ){
                        return model.toJSON();
                    });
                return json;
            } else
            {
                return null;
            }
        }
    
    });

});