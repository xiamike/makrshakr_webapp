define([
    'underscore',
    'backbone',
    'collections/recipeIngredients/RecipeIngredientsCollection'

], function (
    _,
    Backbone,
    RecipeIngredients
) {

    "use strict";

    return Backbone.Model.extend({
    	
    	defaults: {
        },

        initialize: function () {
            //console.log("Init ServerRecipeModel");
            //this.recipeIngredients = new RecipeIngredients();
        },

        parse: function(resp, xhr) {
            //return resp.ingredients;
            console.log("parsing serverRecipe response");
            //console.log(resp);
            //console.log(resp);
            if(resp && resp.ingredientList)
            {
                //console.log(resp.ingredientList);
                
                this.recipeIngredients = new RecipeIngredients(resp.ingredientList);
                //console.log(this.recipeIngredients);
                //console.log(this);
                //console.log(this.recipeIngredients);
                return resp;    
            } else
            {
                return null;
            }
        }

        //Manually serialize object (gives us flexibility when API changes)
        /*
        serialize: function() {
            var object = {};
            object.drinkName = this.get("name");
            object.recipeId  = this.get("recipeId");

            object.email    = this.get("email");
            object.password = this.get("password");

            //Important --> MAKE SURE TO SLICE A NEW ARRAY
            var ingredientsModels = this.recipeIngredients.models.slice(); //Get a new array!

            var actionModel = this.actionIngredient;

            if(actionModel)
            {
                actionModel.set("qty",1);
                ingredientsModels.push(actionModel);
            }

            var strainModel = this.strainIngredient;
            if(strainModel)
            {
                strainModel.set("qty",1);
                ingredientsModels.push(strainModel);
            }

            var payloadIngredientsList = _.map(ingredientsModels,function( model ){
                var payloadObject           = {};
                payloadObject.ingredientId  = model.get("ingredientId");
                //payloadObject.qty         = model.get("volume");
                payloadObject.qty           = model.get("qty");
                return payloadObject;
            });

            object.ingredientList = payloadIngredientsList;
            return JSON.stringify(object);
        },

        canIncreaseIngredient : function (ingredientType, ingredientId) {
            // Get current total volume.
            var currTotalVolume = 0;
            $(this.recipeIngredients.models).each( function (index, item) { 
                currTotalVolume += item.get('qty') 
            });

            var increment = App.conversionMapping[ingredientType] * App.initVolumeMapping[ingredientType];
            if (parseFloat(currTotalVolume + increment) > 200.0) {
                return false;
            } else {
                return true;
            }

        },

        totalVolume : function()
        {
            var currTotalVolume = 0;
            $(this.recipeIngredients.models).each( function (index, item) { 
                currTotalVolume += item.get('qty') 
            });
            return currTotalVolume;
        }*/



    });

 });