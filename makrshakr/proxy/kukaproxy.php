<?php
	$path 			= $_GET['path'];
	$baseUrl 		= 'https://www.makrshakr.com:8081/KUKAWS/';
	$url 			= $baseUrl . $path;

	$ch 			= curl_init($url);
 

	$requestBody 	= file_get_contents('php://input');

	$requestBody 	= str_replace("\n", '', $requestBody);

	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_VERBOSE, true);
	curl_setopt($ch, CURLOPT_HEADER, false);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
 
	$response = curl_exec($ch);
	curl_close($ch);

	header('Content-Type: application/json');
	echo $response;
?>