define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/common/navigationFooterTemplateWithBack.html',
    'text!templates/common/navigationFooterTemplateWithoutBack.html',
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    NavigationFooterTemplateWithBack,
    NavigationFooterTemplateWithoutBack
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {
            this.needsBackButton = options.needsBackButton || false;
            this.nextButtonTitle = options.nextButtonTitle || 'Next';
            this.nextAllowed = options.nextAllowed || true;
            this.nextHref = options.nextHref || '#';

            Handlebars.registerHelper("nextAllowedClass", function () {
                //console.log("invoking helper");
                if(this.nextAllowed==true)
                {
                    return "";
                } else
                {
                    return "ui-disabled";
                }
            });

        },

        toggleNextAllowed: function(trueOrFalse)
        {
            this.nextAllowed = trueOrFalse? true : false;
            this.render();
        },

        render: function () {
            //console.log("Render navigation footer");

            if(this.needsBackButton==true)
            {
                var template    =   Handlebars.compile(NavigationFooterTemplateWithBack);
            } else
            {
                var template    =   Handlebars.compile(NavigationFooterTemplateWithoutBack);
            }


            var data        =   {
                                    nextButtonTitle:    this.nextButtonTitle,
                                    needsBackButton:    this.needsBackButton,
                                    nextAllowed:        this.nextAllowed,
                                    nextHref:           this.nextHref,
                                };

            var el          =   template(data);

            this.$el.html(el);

            //Hack to get page contents styled correctly


            //Need to disable button using javascript
            //See: http://view.jquerymobile.com/1.3.0/docs/widgets/buttons/
            if(this.nextAllowed==true)
            {
                if (this.$("#nextButton").hasClass("ui-disabled"))
                {
                    this.$("#nextButton").removeClass("ui-disabled");
                }
            } else
            {
                if (!this.$("#nextButton").hasClass("ui-disabled"))
                {
                    this.$("#nextButton").addClass("ui-disabled");
                }
            }

            $(".ui-page").trigger("pagecreate");
        },

        postRenderSetup: function() {

        }

    });

});