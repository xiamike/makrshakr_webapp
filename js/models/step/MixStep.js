define([
	'underscore',
	'backbone',
	'models/step/Step'
], function (_, Backbone, Step) {

	"use strict";

	return Step.extend({
		
		defaults: {
            type: "mix"
        }

	});

});