define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'views/common/NavigationHeaderView',
  'views/common/NavigationFooterView',
  'text!templates/order/orderStatusTemplate.html'
], function (
  $,
  jquerymobile,
  _,
  Handlebars,
  Backbone,
  NavigationHeaderView,
  NavigationFooterView,
  CreateSuccessTemplate) {

    "use strict";

    return Backbone.View.extend({

        events: {
            "click  #nextButton"     :"nextButtonPressed"
        },


        initialize: function (options) {
          this.orderStatus = options.orderStatus;
          console.log(this.orderStatus);
        },

        render: function (eventName) {
          console.log("Render create success view");
            var template  = Handlebars.compile(CreateSuccessTemplate);

            //var steps = this.model.recipeIngredients.toJSON();


            var el = template({orderStatus: this.orderStatus});
            this.$el.html(el);
            this.loadSubViews();


            // Show screen for 5 seconds then close.
            console.log("LOGGING OUT.");

            window.setTimeout(function () {
              window.EventDispatcher.trigger("showLoadingUI", "Logging out...");
  
              window.setTimeout(function () {
                $.mobile.loading("hide");
                window.EventDispatcher.trigger("logout");
                }, 
                2000);
  
              }, 
              4000);
        },

        loadSubViews: function() {
            console.log("Create success view load subviews");

            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Thank you !! /////////",
                nav: 'design'
            });
            this.navigationHeaderView.render();

            ////////////////////////////
            /* Footer navigation here */
            ////////////////////////////

            // this.navigationFooterView = new NavigationFooterView({
            //     el: this.$("#navigation-footer-wrapper"),
            //     needsBackButton:    false,
            //     nextButtonTitle:    "+++++ ADD PHONE NUMBER +++++",
            //     nextAllowed:        true
            // });
            // this.navigationFooterView.render();

            ///////////////////////////
            /* End footer navigation */
            ///////////////////////////


            //this.NextButtonView.render();
        },

         postShowSetup: function()
        {
            //this.resizeAndRenderWave();
            //window.EventDispatcher.trigger("errorPopUp", "Thank you for your order!  your drink is now queued and should appear on screen shortly!");

        },

        nextButtonPressed: function (event) {
            var phoneNumber = $("#phonenumber").attr("value");

            if (phoneNumber === "" || isNaN(phoneNumber)) {
                window.EventDispatcher.trigger("errorPopUp", "Please enter a valid number.");
                return false;
            }

            phoneNumber = "+" + phoneNumber;

            var data = {
              userPhoneNumber: phoneNumber,
              email:           App.user.get('email'),
              password:        App.user.get("token"),
            };

            var serializedData = JSON.stringify(data);
            console.log(serializedData);

            var that = this;
            $.post("proxy/kukaproxy.php?path=addPhone", serializedData)

            //Success callback
            .done(function(data, textStatus, jqXHR)
            {
                console.log(data);

                if (parseInt(data.code) > 0)
                {
                    var msg = data.userPhoneNumber + " added successfully. We will text you when your drink is ready.";
                    window.EventDispatcher.trigger("errorPopUp", msg);
                }
                else {
                    window.EventDispatcher.trigger("errorPopUp", "Failed to add phone numer "+textStatus);
                }

            })

            //Failure callback
            .fail(function(jqXHR, textStatus, errorThrown)
            {
                console.log("==========");
                console.log("Failed to add phone numer.");
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
              
                console.log("==========");
                window.EventDispatcher.trigger("errorPopUp", "Failed to add phone numer "+textStatus);
            });

            return false;
            event.preventDefault();
        }

    });

});