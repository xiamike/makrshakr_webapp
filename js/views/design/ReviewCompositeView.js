/**
 * @name Review Composite
 *
 * @description Composite that allows user to view ratio of ingredients and name drink
 */

define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'models/recipe/RecipeModel',
    'text!templates/design/reviewCompositeTemplate.html',
    'views/common/NavigationHeaderView',
    'views/common/NavigationFooterView',
    'views/design/WaveGraphicView',
    'views/design/ReviewListView',
    'views/design/DrinkProgressView',
    'text!templates/design/staticActionStepTemplate.html'

], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    Recipe,
    ReviewCompositeTemplate,
    NavigationHeaderView,
    NavigationFooterView,
    WaveGraphicView,
    ReviewListView,
    DrinkProgressView,
    StaticActionStepTemplate

) {

    "use strict";

    return Backbone.View.extend({
        el: $("#page"),

        initialize: function (options) {
            console.log("init ReviewComposite");
            _.bindAll(this);
           
            //window.addEventListener('resize', this.resizeAndRenderWave, false);
        },

        loadSubViews: function () {

            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Name Your Drink (3/3)"
            });

            this.navigationHeaderView.setElement(this.$('#navigation-header-wrapper')).render();

            //List of drinks
            this.reviewListView = new ReviewListView({
                el: this.$("#recipe-steps-list-wrapper"),
                model: App.recipe
            });
            this.reviewListView.render();


            //Volume bar
            this.drinkProgressView = new DrinkProgressView({
                el: this.$("#drink-progress-wrapper"),
                model: App.recipe
            });
            this.drinkProgressView.render();
            this.drinkProgressView.animate();


            //WaveGraphicView
            //this.waveGraphicView = new WaveGraphicView({
            //    el: this.$("#wave-wrapper")
            //});
            //this.waveGraphicView.render();
            //this.resizeAndRenderWave();

            //Manually append mix action and strain
            this.$("#recipe-actions-list-wrapper").empty();

            if(App.recipe.actionIngredient||App.recipe.strainIngredient)
            {
                this.$("#recipe-actions-list-wrapper").empty();
                //Append instructions
                this.$("#recipe-actions-list-wrapper").append("<div class='small-instructions-2'><p>Preparation method:</p></div>");
                
                //Append ul
                this.$("#recipe-actions-list-wrapper").append("<ul id='action-steps-list' data-role='listview' data-inset='true' data-corners='false'></ul>");

                if(App.recipe.actionIngredient)
                {
                    var template  = Handlebars.compile(StaticActionStepTemplate);
                    var data = App.recipe.actionIngredient.toJSON();
                    var html = template(data);
                    this.$("#action-steps-list").append(html);
                }

                if(App.recipe.strainIngredient)
                {
                    var template  = Handlebars.compile(StaticActionStepTemplate);
                    var data = App.recipe.strainIngredient.toJSON();
                    var html = template(data);
                    this.$("#action-steps-list").append(html);
                }


            }

            ///////////////////////
            /* Footer navigation */
            ///////////////////////

            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    true,
                nextButtonTitle:    "++ Order Drink ++",
                nextAllowed:        true,
                nextHref:           "#order"
            });
            this.navigationFooterView.render();

            //update navigation footer based on collection
            this.listenTo(App.recipe.recipeIngredients, 'change', this.updateNextButton);
            this.listenTo(App.recipe.recipeIngredients, 'add',    this.updateNextButton);
            this.listenTo(App.recipe.recipeIngredients, 'remove', this.updateNextButton);
            
            ///////////////////////////
            /* End footer navigation */
            ///////////////////////////
        },


        render: function () {
            console.log("render ReviewComposite");
            var template  = Handlebars.compile(ReviewCompositeTemplate);

            this.$el.html(template);

            // this.$el.removeClass('login-content-wrapper');

            console.log("================ PAGE CREATE ============");
            $("#page").trigger("pagecreate");
            console.log($("#page").length);
            console.log($("#page"));
            this.loadSubViews();

            return this;
        },


        postShowSetup: function()
        {
            //this.resizeAndRenderWave();
            //window.EventDispatcher.trigger("errorPopUp", "Now name the drink you created!");

            //Get drink name
            $.getJSON(App.routes.getRandomDrinkName, function(data) {
                if(data.message) {
                    $('#name').val(data.message);
                    App.recipe.set("name", data.message);
                }
            });

            $("#page").trigger("pagecreate");

        },


         /*----- Wave height logic -----*/
        resizeAndRenderWave: function() {
            console.log(App.recipe);
            var ingredients = App.recipe.recipeIngredients.getIngredientsOnly();
            var numIngredients = ingredients.length;

            //Calculate height of ingredient elements
            //Make sure to account for border heights
            var totalHeight = 1;
            var that = this;
            $(ingredients).each(function(index,element){
                var volume = element.get("volume");
                var type = element.get('type');
                var incrementUnit = App.initVolumeMapping[type];

                var height = 0 + parseFloat(volume / incrementUnit) * (44)+1;

                totalHeight+=height;
            });
            var height = totalHeight;

            //Important! resize the canvas
            this.$("#waveCanvas")[0].width = this.$("#wave-wrapper").width()-1;
            console.log(this.$("#waveCanvas").width());
            console.log(this.$("#wave-wrapper").width());


            this.waveGraphicView.renderWave(height-8);
        },


        events:{
            "input          #name"          :"nameDidChange",
            "click          #nextButton"    :"clickedCreate",
        },


        clickedCreate: function(){
            //console.log("clickedCreate");
            window.EventDispatcher.trigger("createRecipe");
            event.preventDefault(); //prevent linking
        },

        nameDidChange: function (event){
            console.log("nameDidChange");
            var name = $(event.target).attr("value");
            console.log(name);
            App.recipe.set("name",name);
            if(name.length>0)
            {
                this.navigationFooterView.toggleNextAllowed(true);
            } else
            {
                this.navigationFooterView.toggleNextAllowed(false);
            }
        },

    });

});