define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'collections/drinks/DrinksCollection',
    'models/drink/DrinkModel',
    'text!templates/explore/exploreFilterDialogTemplate.html',
], function (
    $,
    _,
    Backbone,
    Handlebars,
    DrinksCollection,
    DrinkModel,
    ExploreFilterDialogTemplate) {

    "use strict";

    return Backbone.View.extend({

        events: {
            "click        #apply-filters"          : "applyFilters",

            "change       #gender-filter input"    : "changeGender",

            "change       #age-filter input"       : "changeAge",

            "change       #alcohol-filter input"   : "changeAlcohol",

            "change       #spirit-filter select"   : "changeSpirit",

            "change       #mixer-filter select"    : "changeMixer"

        },

        applyFilters: function(event){
            event.preventDefault(); //prevent linking
            console.log("Apply filters");
            console.log(JSON.stringify(this.filters));

            //Dispatch close event
            window.EventDispatcher.trigger("exploreFilterDialog:close", this.filters);
        },
        
        changeGender: function(event){
            var isChecked = $(event.currentTarget).is(':checked');
            var id = $(event.currentTarget).attr("id")
            this.filters.gender[id] = isChecked ? true : false;
        },

        changeAge: function(event){
            var id = $(event.currentTarget).attr("id")
            var value = $(event.currentTarget).val()
            this.filters.age[id] = value;
        },

        changeAlcohol: function(event){
            var id = $(event.currentTarget).attr("id")
            var value = $(event.currentTarget).val()
            this.filters.alc[id] = value;
        },

        changeSpirit: function(event){
            var id = $(event.currentTarget).attr("id");
            var value = $(event.currentTarget).val();
            var text = $(event.currentTarget).children("option:selected").text();
            this.filters.spirit = value;

        },

        changeMixer: function(event){
            var id = $(event.currentTarget).attr("id");
            var value = $(event.currentTarget).val();
            var text = $(event.currentTarget).children("option:selected").text();
            this.filters.mixer = value;
        },


        initialize: function(){

            //Helper for Checkbox HTML markup
            Handlebars.registerHelper("checkbox", function (item) {
                console.log(item);
                return item == true ? "checked" : "";
            });


            //Setup default filters
            //Todo: move to a class / Backbone model?
            this.filters                = new Object();
            
            this.filters.gender         = new Object();
            this.filters.gender.male    = true;
            this.filters.gender.female  = false;

            this.filters.age            = new Object();
            this.filters.age["min-age"] = 18;
            this.filters.age["max-age"] = 60;

            this.filters.alc            = new Object();
            this.filters.alc["min-alc"] = 20;
            this.filters.alc["max-alc"] = 60;
        },

        render: function (eventName) {
            var template  = Handlebars.compile(ExploreFilterDialogTemplate);

            console.log(JSON.stringify(this.filters));
            this.filters.test = "test";
            var el = template({"filters": this.filters});
            this.$el.html(el);

            //this.$el.html(template);
            $("#page").trigger("pagecreate");
            this.loadSubViews();
            return this;
        },

        loadSubViews: function() {
           
        },

    });

});
