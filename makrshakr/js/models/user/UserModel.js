define([
    'underscore',
    'backbone',
], function (_, Backbone) {

    "use strict";

    return Backbone.Model.extend({
    	
    	defaults: {
        },

        initialize: function () {
            
        },

        //Checks whether user token AND email are set
        isSet: function() {
            var token = this.get("token"),
                email = this.get("email"),
                userId = this.get("userId"),
                userType = this.get("userType"),
                tokenSet = false,
                emailSet = false,
                userIdSet = false,
                userTypeSet = false;

            if(token && typeof token == 'string' && token.length>0)
            {
                tokenSet = true;
            }

            if(email && typeof email == 'string' && email.length>0)
            {
                emailSet = true;
            }

            if(userId) //check type?
            {
                userIdSet = true;
            }

            if(userType) //check type?
            {
                userTypeSet = true;
            }


            if(tokenSet && emailSet && userIdSet && userTypeSet)
            {
                console.log("UserModel >> User details are set.");
                console.log(this);
                return true;
            } else
            {
                console.log("UserModel >> User details are NOT set.")
                return false;
            }
        },

        //Try populating user details from sessionStorage
        tryPullFromStorage: function() {
            if(typeof(Storage)!=="undefined")
            {
                var email = sessionStorage.email,
                    token = sessionStorage.token,
                    userId = sessionStorage.userId,
                    userType = sessionStorage.userType,
                    tokenSet = false,
                    emailSet = false,
                    userIdSet = false,
                    userTypeSet = false;
                
                if(token && typeof token == 'string' && token.length>0)
                {
                    tokenSet = true;
                }

                if(email && typeof email == 'string' && email.length>0)
                {
                    emailSet = true;
                }

                if(userId) //Check type?
                {
                    userIdSet = true;
                }

                if(userType) //Check type?
                {
                    userTypeSet = true;
                }

                if(tokenSet && emailSet && userIdSet && userTypeSet)
                {
                    //Populate user object with sessionStorage details
                    this.set("token",token);
                    this.set("email",email);
                    this.set("userId",userId);
                    this.set("userType",userType);
                    return true;
                } else
                {
                    return false;
                }
            }
            else
            {
                //sessionStorage is not available
                return false;
            }
        },

        //Checks whether details are set
        isUserInfoSet: function() {
            var userImageUrl = this.get("userImageUrl"),
                    userName = this.get("userName"),
                    gender = this.get("gender"),
                    age = this.get("age"),
                    nationality = this.get("nationality"),
                    
                    userImageUrlSet = false,
                    userNameSet = false,
                    genderSet = false,
                    ageSet = false,
                    nationalitySet = false;


            if(userImageUrl && typeof userImageUrl == 'string' && userImageUrl.length>0)
            {
                userImageUrlSet = true;
            }

            if(userName && typeof userName == 'string' && userName.length>0)
            {
                userNameSet = true;
            }

            if(gender && typeof gender == 'string' && gender.length>0)
            {
                genderSet = true;
            }

            if(age && typeof age == 'string' && age.length>0)
            {
                ageSet = true;
            }

            if(nationality && typeof nationality == 'string' && nationality.length>0)
            {
                nationalitySet = true;
            }

            // NLAM
            /*
            if(userImageUrlSet
                && userNameSet
                && genderSet
                && ageSet
                && nationalitySet)
            {
                console.log("UserModel >> User bio details are set.");
                console.log(this);
                return true;
            } else
            {
                console.log("UserModel >> User bio details are NOT set.")
                return false;
            }
            */

        },

        //Try populating user details from sessionStorage
        tryPullUserInfoFromStorage: function() {
            if(typeof(Storage)!=="undefined")
            {
                var userImageUrl = sessionStorage.userImageUrl,
                    userName = sessionStorage.userName,
                    gender = sessionStorage.gender,
                    age = sessionStorage.age,
                    nationality = sessionStorage.nationality,
                    
                    userImageUrlSet = false,
                    userNameSet = false,
                    genderSet = false,
                    ageSet = false,
                    nationalitySet = false;
                
                if(userImageUrl && typeof userImageUrl == 'string' && userImageUrl.length>0)
                {
                    userImageUrlSet = true;
                }

                if(userName && typeof userName == 'string' && userName.length>0)
                {
                    userNameSet = true;
                }

                if(gender && typeof gender == 'string' && gender.length>0)
                {
                    genderSet = true;
                }

                if(age && typeof age == 'string' && age.length>0)
                {
                    ageSet = true;
                }

                if(nationality && typeof nationality == 'string' && nationality.length>0)
                {
                    nationalitySet = true;
                }

                if(userImageUrlSet
                    && userNameSet
                    && genderSet
                    && ageSet
                    && nationalitySet)
                {
                    //console.log("UserModel >> User bio details are set.");
                    //console.log(this);
                    //Populate user object with sessionStorage details
                    this.set("userImageUrl", userImageUrl);
                    this.set("userName", userName);
                    this.set("gender", gender);
                    this.set("age", age);
                    this.set("nationality", nationality);



                    return true;
                } else
                {
                    //console.log("UserModel >> User bio details are NOT set.")
                    return false;
                }
            }
            else
            {
                //sessionStorage is not available
                return false;
            }
        }




    });

 });