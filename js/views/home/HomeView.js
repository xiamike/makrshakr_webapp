define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/home/homeTemplate.html'
], function ($, _, Backbone, homeTemplate) {

    "use strict";

    return Backbone.View.extend({

        el: $("#page"),

        render: function (eventName) {
            //$('.menu li').removeClass('active');
            //$('.menu li a[href="#"]').parent().addClass('active');
            this.$el.html(homeTemplate);
            //this.$el.trigger("create");
            this.$el.addClass('login-content-wrapper');
            this.$el.trigger("pagecreate");
            //Need to trigger to refresh elements
            //Or manually apply CSS
            //https://github.com/jquery/jquery-mobile/issues/2703
        }

    });

});
