define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'text!templates/login/createEmailTemplate.html',
  'views/common/NavigationFooterView',
  'models/user/UserModel'
], function(
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    CreateEmailTemplate,
    NavigationFooterView,
    User
    ) {

    "use strict";

    return Backbone.View.extend({

        events:{
            "click          #nextButton"     :"nextButtonPressed"
        },

        initialize: function (options) {
            _.bindAll(this);

            this.newUserObject = new User();

        },

        render: function (eventName) {
            var template  = Handlebars.compile(CreateEmailTemplate);
            this.$el.html(template);
            this.$el.addClass('login-content-wrapper');
            this.loadSubViews();
        },

        loadSubViews: function() {
            console.log("CreateEmailView >> load subviews");

            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    true,
                nextButtonTitle:    "Create",
                nextAllowed:        true
            });
            this.navigationFooterView.render();
        },

        nextButtonPressed: function (event) {
            //Set email
            var email = this.$("#email").attr("value").toLowerCase();
            this.newUserObject.set("email", email);

            //Set password
            var password = this.$("#password").attr("value").toLowerCase();
            this.newUserObject.set("password", password);

            var that = this;

            window.EventDispatcher.trigger("showLoadingUI", "Creating account");

            var serializedUserData = JSON.stringify(this.newUserObject);

            $.post("proxy/kukaproxy.php?path=register",
                serializedUserData)
            //Success handler
            .done( function(data, textStatus, jqXHR)
            {
                console.log("==========");
                console.log("CreateEmailView >> Register user response: ");
                console.log("data: ");
                console.log(data);
                console.log("textStatus: " + textStatus);
                console.log("==========");

                var successCode = data.success;
                successCode = parseInt(successCode,10);

                //True success
                if (successCode && successCode > 0) {
                    console.log("CreateEmailView >> Register success");

                    //Email that we just submitted
                    var email = that.newUserObject.get("email");
                    var token = data.message;
                    var userId = data.userId;
                    var userType = 0;
                    
                    //////////SB: NEW CODE////////////
                    window.EventDispatcher.trigger("createUserSuccess", email, token, userId, userType);

                   /* window.EventDispatcher.trigger(
                        "resetUser",
                        email,
                        token,
                        userId,
                        userType
                    );

                    //Trigger profile edit route
                    window.router.navigate('profileEdit', true);*/




                //Server response code is failure
                } else {
                    var message = data.message;
                    window.EventDispatcher.trigger("errorPopUp", "Register failed: " +data.message);
                    console.log("==========");
                    console.log("Register user response: ");
                    console.log("data:");
                    console.log(data);
                    console.log("textStatus: " + textStatus);
                    console.log("==========");
                }
            })

            //Failure callback
            .fail(function(jqXHR, textStatus, errorThrown)
            {
                console.log("CreateEmailView >> Failed to register user via email.");
                console.log("==========");
                console.log("Register user response: ");
                console.log("errorThrown: " + errorThrown);
                console.log("textStatus: " + textStatus);
                console.log("==========");
                 window.EventDispatcher.trigger("errorPopUp", "Server error: " +textStatus);
            });

            event.preventDefault();
        
        } //End next button handler


    });

});