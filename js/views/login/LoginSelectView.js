define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/login/loginSelectTemplate.html',
    'views/common/NavigationFooterView'
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    LoginSelectTemplate,
    NavigationFooterView) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {
            _.bindAll(this);

        },

        render: function (eventName) {
            var template  = Handlebars.compile(LoginSelectTemplate);
            var html = template(this.pageData);
            this.$el.addClass('login-content-wrapper');
            this.$el.html(html);
            this.loadSubViews();
        },

        loadSubViews: function() {

            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    true,
                nextButtonTitle:    "",
                nextAllowed:        false,
                nextHref:           "#loginEmail"
            });
            this.navigationFooterView.render();
        }

    });

});