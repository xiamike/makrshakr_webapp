define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/extra/cokeIngredientTemplate.html',
    'views/common/NavigationHeaderView',
], function ($, jquerymobile, _, Handlebars, Backbone, CokeIngredientTemplate, NavigationHeaderView) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {
        },

        render: function (eventName) {
            var template  = Handlebars.compile(CokeIngredientTemplate);
            this.$el.html(template);
            this.loadSubViews();
        },

        loadSubViews: function() {
            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Coca Cola Information"
            });
            this.navigationHeaderView.render();
        }

    });

});