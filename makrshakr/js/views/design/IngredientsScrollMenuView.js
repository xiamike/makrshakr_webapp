define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/design/ingredientsScrollMenuTemplate.html'
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    IngredientsScrollMenuTemplate
) {

    "use strict";

    var scrollView;

    return Backbone.View.extend({

        initialize: function() {
            this.render();
        },

        render: function () {
            //Add line breaks to ingredient names
            _.each(this.options.data.items, function(ingredient) {
                ingredient.ingredientName = ingredient.ingredientName.replace('~', '<br>');
            });

            var el = Handlebars.compile(IngredientsScrollMenuTemplate),
                scrollViewHTML = el(this.options.data);

            this.$el.html(scrollViewHTML);
            this.resizeScroller();
            this.initScroller();

            this.scrollScroller();
        },

        resizeScroller: function() {
            var scrollEl = this.$("#scroll-view ul li"),
                numItems = this.options.data.items.length,
                //width = parseInt(scrollEl.css("width")), 
                //margin = parseInt(scrollEl.css("margin-left")),
                //totalWidth = numItems * width + (numItems+1) * margin;
                totalWidth = numItems * 97;

            $("#scroll-view").css("width", totalWidth);
        },

        initScroller: function() {
            var s = this.$("#scroll-wrapper")[0];
            this.scrollView = new iScroll(s, {
                hScrollbar      : false, 
                vScrollbar      : false, 
                fixedScrollbar  : true,
                vScroll         : false,
            });

            //use vmousemove for testing (captures both mouse and touch moves)
            $(document).bind('vmousemove', function (e) {
                //console.log(e.target.id);
                if (e.target.id == "scroll-wrapper") {
                    e.preventDefault();
                }
            });
            var that = this;
            setTimeout(function () { that.scrollView.refresh(); }, 0);

        },


        scrollScroller: function() {
            $('#scroll-wrapper')
                .delay('2000')
                .scrollTo('25px', {duration: 500, axis: 'x'})
                .scrollTo('0px', {duration: 100, axis: 'x'});
        }

    });

});