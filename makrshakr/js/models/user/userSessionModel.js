define([
    'underscore',
    'backbone',
], function (_, Backbone) {

    "use strict";

    return Backbone.Model.extend({
    	
    	defaults: {
        },

        initialize: function () {
        }

        url: function () {
            return 'https://www.makrshakr.com:8081/KUKAWS/login';
        }


    });

 });