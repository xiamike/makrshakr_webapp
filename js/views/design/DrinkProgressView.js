define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/design/drinkProgressTemplate.html'
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    DrinkProgressTemplate
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function() {
            this.render();
        },

        render: function () {
            var el = Handlebars.compile(DrinkProgressTemplate),
            
            html = el({});

            this.$el.html(html);
        },

        animate: function() {
            var totalVolume = this.model.totalVolume();
            if(isNaN(totalVolume))
            {
                totalVolume = 0.0;
            }
            var maxVolume = 200.0;
            
            var percentage = totalVolume/200.0;

            var percentageText = percentage*100+"%";
            this.$("#progress-bar").stop(); //stop existing animations
            this.$("#progress-bar").animate({
                width: percentageText
            }, 500);

            var percentageDisplay = Math.floor(percentage.toFixed(2) * 100);
            var progressText = totalVolume+" / 200ml "+"("+percentageDisplay+"%)";
            this.$("#progress-text p").text(progressText);


        }

    });

});