define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'views/common/NavigationHeaderView',
  'views/common/NavigationFooterView',
  'text!templates/design/designHomeTemplate.html'
], function (
  $,
  jquerymobile,
  _,
  Handlebars,
  Backbone,
  NavigationHeaderView,
  NavigationFooterView,
  DesignHomeTemplate
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {
        },

        render: function (eventName) {
          console.log("Render design home view");
            var template  = Handlebars.compile(DesignHomeTemplate);
            this.$el.html(template);
            this.$el.addClass('design-instructions');
            this.loadSubViews();
        },

        loadSubViews: function() {
            console.log("Design home view load subviews");
            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$('#navigation-header-wrapper'),
                title: 'Design A Drink',
                nav: 'design'
            });
            this.navigationHeaderView.render();

            ////////////////////////////
            /* Footer navigation here */
            ////////////////////////////

            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    false,
                nextButtonTitle:    "Okey Dokey",
                nextAllowed:        true,
                nextHref:           "#ingredients"
            });
            this.navigationFooterView.render();

            ///////////////////////////
            /* End footer navigation */
            ///////////////////////////

        },

    });

});