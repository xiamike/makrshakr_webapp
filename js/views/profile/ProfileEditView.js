define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/profile/profileEditTemplate.html',
    'views/common/NavigationFooterView',
    'views/profile/AvatarDialogView'
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    ProfileEditTemplate,
    NavigationFooterView,
    AvatarDialogView)
{

   "use strict";

    return Backbone.View.extend({

        events: {
            "change       #gender-selector input"        : "changeGender",
            //"input        #username"                     : "changeUserName",
            "click        #nextButton"                   : "nextButtonPressed",
            "click        #edit-profile-btn"             : "editProfilePressed"
        },

        initialize: function (options) {
            this.filters                = new Object();
            
            this.filters.gender         = new Object();
            this.filters.gender.male    = false;
            this.filters.gender.female  = true;

            if (this.model.get('userImageUrl') == undefined) {
                this.model.set('userImageUrl', 'http://www.makrshakr.com/app/images/userImages/avatar-01.png');
            }
        },

        render: function (eventName) {
            var template  = Handlebars.compile(ProfileEditTemplate);

            console.log(this.model);

            var countries = this.countryString().countries;

            var html = template({"user": this.model.toJSON(), "countries": countries});
            this.$el.html(html);

            this.loadSubViews();

            // TODO: Refactor male/female logic.
            if (this.model.get('gender') == "female") {
                this.filters.gender.male   = false;
                this.filters.gender.female = true;
            } else if (this.model.get('gender') == "male") {
                this.filters.gender.male   = true;
                this.filters.gender.female = false;
            }

            if (this.filters.gender.female) {
                this.$("#female").attr('checked', true);
            } else {
                this.$("#male").attr('checked', true);
            }

            this.$("#age").val(this.model.get('age'));
            this.$("#nationality").val(this.model.get('nationality'));

            this.renderProfileHexagaon();
        },
         
       //Generate array of hexagon points given center X and Y and a radius
        hexagonPoints: function(centerX, centerY, radius){
            var angleStep = 2*Math.PI/6;
            var points = [];
            for(var i = 0; i<6; i++)
            {
                var x = centerX + Math.cos(angleStep*i+angleStep/2)*radius;
                var y = centerY + Math.sin(angleStep*i+angleStep/2)*radius;
                var point = [];
                point["x"] = x;
                point["y"] = y;
                points[i]=point;
            }
            return points;
        },

        loadSubViews: function() {
            ////////////////////////////
            /* Footer navigation here */
            ////////////////////////////

            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    false,
                nextButtonTitle:    "Submit",
                nextAllowed:        true
            });
            this.navigationFooterView.render();
        },

        changeUserName: function(event){
            var userName    = $("#username").attr("value");
            console.log(userName);
        },

        changeGender: function(event){
            var isChecked = $(event.currentTarget).is(':checked');
            var id = $(event.currentTarget).attr("id")

            if (id == "female") {
                this.filters.gender.male   = false;
                this.filters.gender.female = true;
            } else {
                this.filters.gender.male   = true;
                this.filters.gender.female = false;
            }

            this.renderProfileHexagaon();
        },

        // Create new account.
        nextButtonPressed: function(event) {

            console.log(">>>>>>>>>>>>>>>>");
            console.log("Attempting to update profile");
            console.log(this.model.get('email'));

            var userName    = $("#username").attr("value");
            var age         = $("#age").attr("value");
            var nationality = $("#nationality").attr("value");
            var gender      = ($('form #female').is(':checked')) ? "female" : "male";

            age = parseInt(age);
            if(age<18)
            {
                window.EventDispatcher.trigger("errorPopUp","You must be at least 18 to participate.");
                return false;
            }

            if (userName    === ""   ||
                age         === ""   || 
                nationality === "0"  ||
                gender      === ""   ||
                this.model.get('userImageUrl') === "") {
                window.EventDispatcher.trigger("errorPopUp", "Please fill in all fields to continue.");
                return false;
                event.preventDefault();            
            }

            this.model.set('userName',    userName);
            this.model.set('age',         age);
            this.model.set('nationality', nationality);
            this.model.set('gender',      gender);
            this.model.set('password', this.model.get('token'));

            var that = this;

            //console.log(JSON.stringify(this.model));

            var serializedData = JSON.stringify(this.model);

            //Success handler
            $.post("proxy/kukaproxy.php?path=updateUserInfo", serializedData)

            .done(function(data, textStatus, jqXHR) {
                console.log("==========");
                console.log("Update profile response: ");
                console.log(data);
                console.log(textStatus);
                console.log("==========");

                 /*IMPORTANT*/
                //On register, success returned in data.success = 1
                //On login, success is returned in data.code = 1
                var successCode = data.code;
                successCode = parseInt(successCode,10);
                console.log(successCode);

                if (successCode == -1) {
                    var message = data.message;
                    window.EventDispatcher.trigger("errorPopUp", "Update user information failed: " +data.message);
                    console.log("==========");
                    console.log("Update profile response: ");
                    console.log(data);
                    console.log(textStatus);
                    console.log(data);
                    console.log("==========");
                } else {

                    //SB:
                    //!Important!
                    //Here we only send the response data (vs. sending user object in login)
                    //The server response will NOT include the user object.
                    //this.model refers to App.user, so user details are already set here
                    //We might refactor this to use the same approach as Login (use temporary user object)
                    window.EventDispatcher.trigger(
                        "updateProfileSuccess",
                        data);
                }

            }) //End success handler

            //Failure callback
            .fail(function(event, jqxhr, settings, exception)
            {
                console.log("Failed to update user information");
                window.EventDispatcher.trigger("errorPopUp", "Server error: " +exception);

                //SB: We've failed here, so make sure to clear the attributes that we've set
                this.model.unset('userName');
                this.model.unset('age');
                this.model.unset('nationality');
                this.model.unset('gender');
                this.model.unset('password');
            });









            event.preventDefault();            
        },

        editProfilePressed: function (event) {
            console.log("profile edit");

            // Save current data.
            var userName    = $("#username").attr("value");
            var age         = $("#age").attr("value");
            var nationality = $("#nationality").attr("value");
            var gender      = ($('form #female').is(':checked')) ? "female" : "male";
            this.model.set('userName',    userName);
            this.model.set('age',         age);
            this.model.set('nationality', nationality);
            this.model.set('gender',      gender);

            var view = new AvatarDialogView({model: this.model});
            this.pushDialog(view);
        },

        pushDialog: function (dialog) {
            $(dialog.el).attr('data-role', 'dialog');
            $(dialog.el).attr('data-close-btn', 'none'); //remove default close button
            dialog.render();

            $('body').append($(dialog.el));

            var transition = "pop";

            $.mobile.changePage($(dialog.el), {changeHash:false, role: 'dialog', transition: transition});
        },

        renderProfileHexagaon: function () {
            var profileHexgon;

            if (this.filters.gender.female) {
                profileHexgon = "images/hexagon-profile-female.svg";
            } else {
                profileHexgon = "images/hexagon-profile-male.svg";
            }


            this.$("#profile-hexagon").empty();

            var drawContainer = d3.select(this.$("#profile-hexagon")[0]);

            var width =         120;
            var height =        120;
            var hexRadius =     52;

            var svgContainer = drawContainer.append("svg")
                .attr("width",width)
                .attr("height",height);

            //Helper function for constructing lines
            var lineFunction = d3.svg.line()
                .x(function(d) { return d.x; })
                .y(function(d) { return d.y; })
                .interpolate("linear");

            //Set clipping path
            var defs = svgContainer.append('svg:defs');
            var that = this;
            var hexagonClipPath = defs.append('clipPath')
                .attr('id','hex-clip-path')
                .append("path")
                .attr("d", function(d){
                    var x = width/2;
                    var y = width/2;
                    var points = that.hexagonPoints(x,y,hexRadius);
                    return lineFunction(points)+"Z";
                });


           var baseLayer = svgContainer.append("image")
                    .attr('xlink:href', this.model.get('userImageUrl'))
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  width)
                    .attr('height', height)
                    .attr("clip-path","url(#hex-clip-path)");

            var overlay = svgContainer.append("image")
                    .attr('xlink:href', profileHexgon)
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  width)
                    .attr('height', height);
        },

        /*
        countryStrings: function () {
            $.ajax({
                async: false
            });

            var countries;
            $.getJSON('data/countries.json', function(data) { 
                countries = data.countries;
            });
    
            console.log(countries);
            return countries;
        }, 
    */

        countryString: function () {
    
                return {

                    "countries":[

                {"code":"AF", "country": "Afghanistan"},
                {"code":"AL", "country": "Albania"},
                {"code":"DZ", "country": "Algeria"},
                {"code":"AS", "country": "American Samoa"},
                {"code":"AD", "country": "Andorra"},
                {"code":"AO", "country": "Angola"},
                {"code":"AI", "country": "Anguilla"},
                {"code":"AQ", "country": "Antarctica"},
                {"code":"AG", "country": "Antigua and Barbuda"},
                {"code":"AR", "country": "Argentina"},
                {"code":"AM", "country": "Armenia"},
                {"code":"AW", "country": "Aruba"},
                {"code":"AU", "country": "Australia"},
                {"code":"AT", "country": "Austria"},
                {"code":"AZ", "country": "Azerbaijan"},
                {"code":"BS", "country": "Bahamas"},
                {"code":"BH", "country": "Bahrain"},
                {"code":"BD", "country": "Bangladesh"},
                {"code":"BB", "country": "Barbados"},
                {"code":"BY", "country": "Belarus"},
                {"code":"BE", "country": "Belgium"},
                {"code":"BZ", "country": "Belize"},
                {"code":"BJ", "country": "Benin"},
                {"code":"BM", "country": "Bermuda"},
                {"code":"BT", "country": "Bhutan"},
                {"code":"BO", "country": "Bolivia"},
                {"code":"BA", "country": "Bosnia and Herzegovina"},
                {"code":"BW", "country": "Botswana"},
                {"code":"BV", "country": "Bouvet Island"},
                {"code":"BR", "country": "Brazil"},
                {"code":"IO", "country": "British Indian Ocean Territory"},
                {"code":"BN", "country": "Brunei Darussalam"},
                {"code":"BG", "country": "Bulgaria"},
                {"code":"BF", "country": "Burkina Faso"},
                {"code":"BI", "country": "Burundi"},
                {"code":"KH", "country": "Cambodia"},
                {"code":"CM", "country": "Cameroon"},
                {"code":"CA", "country": "Canada"},
                {"code":"CV", "country": "Cape Verde"},
                {"code":"KY", "country": "Cayman Islands"},
                {"code":"CF", "country": "Central African Republic"},
                {"code":"TD", "country": "Chad"},
                {"code":"CL", "country": "Chile"},
                {"code":"CN", "country": "China"},
                {"code":"CX", "country": "Christmas Island"},
                {"code":"CC", "country": "Cocos (Keeling Islands)"},
                {"code":"CO", "country": "Colombia"},
                {"code":"KM", "country": "Comoros"},
                {"code":"CG", "country": "Congo "},
                {"code":"CK", "country": "Cook Islands"},
                {"code":"CR", "country": "Costa Rica"},
                {"code":"CI", "country": "Cote D'Ivoire (Ivory Coast)"},
                {"code":"HR", "country": "Croatia (Hrvatska"},
                {"code":"CU", "country": "Cuba"},
                {"code":"CY", "country": "Cyprus"},
                {"code":"CZ", "country": "Czech Republic"},
                {"code":"DK", "country": "Denmark"},
                {"code":"DJ", "country": "Djibouti"},
                {"code":"DM", "country": "Dominica"},
                {"code":"DO", "country": "Dominican Republic"},
                {"code":"TP", "country": "East Timor"},
                {"code":"EC", "country": "Ecuador"},
                {"code":"EG", "country": "Egypt"},
                {"code":"SV", "country": "El Salvador"},
                {"code":"GQ", "country": "Equatorial Guinea"},
                {"code":"ER", "country": "Eritrea"},
                {"code":"EE", "country": "Estonia"},
                {"code":"ET", "country": "Ethiopia"},
                {"code":"FK", "country": "Falkland Islands (Malvinas)"},
                {"code":"FO", "country": "Faroe Islands"},
                {"code":"FJ", "country": "Fiji"},
                {"code":"FI", "country": "Finland"},
                {"code":"FR", "country": "France"},
                {"code":"FX", "country": "France, Metropolitan"},
                {"code":"GF", "country": "French Guiana"},
                {"code":"PF", "country": "French Polynesia"},
                {"code":"TF", "country": "French Southern Territories"},
                {"code":"GA", "country": "Gabon"},
                {"code":"GM", "country": "Gambia"},
                {"code":"GE", "country": "Georgia"},
                {"code":"DE", "country": "Germany"},
                {"code":"GH", "country": "Ghana "},
                {"code":"GI", "country": "Gibraltar"},
                {"code":"GR", "country": "Greece"},
                {"code":"GL", "country": "Greenland"},
                {"code":"GD", "country": "Grenada"},
                {"code":"GP", "country": "Guadeloupe"},
                {"code":"GU", "country": "Guam"},
                {"code":"GT", "country": "Guatemala"},
                {"code":"GN", "country": "Guinea"},
                {"code":"GW", "country": "Guinea-Bissau"},
                {"code":"GY", "country": "Guyana"},
                {"code":"HT", "country": "Haiti"},
                {"code":"HM", "country": "Heard and McDonald Islands"},
                {"code":"HN", "country": "Honduras"},
                {"code":"HK", "country": "Hong Kong"},
                {"code":"HU", "country": "Hungary"},
                {"code":"IS", "country": "Iceland"},
                {"code":"IN", "country": "India"},
                {"code":"ID", "country": "Indonesia"},
                {"code":"IR", "country": "Iran"},
                {"code":"IQ", "country": "Iraq"},
                {"code":"IE", "country": "Ireland"},
                {"code":"IL", "country": "Israel"},
                {"code":"IT", "country": "Italy"},
                {"code":"JM", "country": "Jamaica"},
                {"code":"JP", "country": "Japan"},
                {"code":"JO", "country": "Jordan"},
                {"code":"KZ", "country": "Kazakhstan"},
                {"code":"KE", "country": "Kenya"},
                {"code":"KI", "country": "Kiribati"},
                {"code":"KP", "country": "Korea (North)"},
                {"code":"KR", "country": "Korea (South)"},
                {"code":"KW", "country": "Kuwait"},
                {"code":"KG", "country": "Kyrgyzstan"},
                {"code":"LA", "country": "Laos"},
                {"code":"LV", "country": "Latvia"},
                {"code":"LB", "country": "Lebanon"},
                {"code":"LS", "country": "Lesotho"},
                {"code":"LR", "country": "Liberia"},
                {"code":"LY", "country": "Libya"},
                {"code":"LI", "country": "Liechtenstein"},
                {"code":"LT", "country": "Lithuania"},
                {"code":"LU", "country": "Luxembourg"},
                {"code":"MO", "country": "Macau"},
                {"code":"MK", "country": "Macedonia"},
                {"code":"MG", "country": "Madagascar"},
                {"code":"MW", "country": "Malawi"},
                {"code":"MY", "country": "Malaysia"},
                {"code":"MV", "country": "Maldives"},
                {"code":"ML", "country": "Mali"},
                {"code":"MT", "country": "Malta"},
                {"code":"MH", "country": "Marshall Islands"},
                {"code":"MQ", "country": "Martinique"},
                {"code":"MR", "country": "Mauritania"},
                {"code":"MU", "country": "Mauritius"},
                {"code":"YT", "country": "Mayotte"},
                {"code":"MX", "country": "Mexico"},
                {"code":"FM", "country": "Micronesia"},
                {"code":"MD", "country": "Moldova"},
                {"code":"MC", "country": "Monaco"},
                {"code":"MN", "country": "Mongolia"},
                {"code":"MS", "country": "Montserrat"},
                {"code":"MA", "country": "Morocco"},
                {"code":"MZ", "country": "Mozambique"},
                {"code":"MM", "country": "Myanmar"},
                {"code":"NA", "country": "Namibia"},
                {"code":"NR", "country": "Nauru"},
                {"code":"NP", "country": "Nepal"},
                {"code":"NL", "country": "Netherlands"},
                {"code":"AN", "country": "Netherlands Antilles"},
                {"code":"NC", "country": "New Caledonia"},
                {"code":"NZ", "country": "New Zealand"},
                {"code":"NI", "country": "Nicaragua"},
                {"code":"NE", "country": "Niger"},
                {"code":"NG", "country": "Nigeria"},
                {"code":"NU", "country": "Niue"},
                {"code":"NF", "country": "Norfolk Island"},
                {"code":"MP", "country": "Northern Mariana Islands"},
                {"code":"NO", "country": "Norway"},
                {"code":"OM", "country": "Oman"},
                {"code":"PK", "country": "Pakistan"},
                {"code":"PW", "country": "Palau"},
                {"code":"PA", "country": "Panama"},
                {"code":"PG", "country": "Papua New Guinea"},
                {"code":"PY", "country": "Paraguay"},
                {"code":"PE", "country": "Peru"},
                {"code":"PH", "country": "Philippines"},
                {"code":"PN", "country": "Pitcairn"},
                {"code":"PL", "country": "Poland"},
                {"code":"PT", "country": "Portugal"},
                {"code":"PR", "country": "Puerto Rico"},
                {"code":"QA", "country": "Qatar"},
                {"code":"RE", "country": "Reunion"},
                {"code":"RO", "country": "Romania"},
                {"code":"RU", "country": "Russian Federation"},
                {"code":"RW", "country": "Rwanda"},
                {"code":"KN", "country": "Saint Kitts and Nevis"},
                {"code":"LC", "country": "Saint Lucia"},
                {"code":"VC", "country": "Saint Vincent and The Grenadines"},
                {"code":"WS", "country": "Samoa"},
                {"code":"SM", "country": "San Marino"},
                {"code":"ST", "country": "Sao Tome and Principe"},
                {"code":"SA", "country": "Saudi Arabia"},
                {"code":"SN", "country": "Senegal"},
                {"code":"SC", "country": "Seychelles"},
                {"code":"SL", "country": "Sierra Leone"},
                {"code":"SG", "country": "Singapore"},
                {"code":"SK", "country": "Slovak Republic"},
                {"code":"SI", "country": "Slovenia"},
                {"code":"SB", "country": "Solomon Islands"},
                {"code":"SO", "country": "Somalia"},
                {"code":"ZA", "country": "South Africa"},
                {"code":"GS", "country": "S. Georgia and S. Sandwich Isls."},
                {"code":"ES", "country": "Spain"},
                {"code":"LK", "country": "Sri Lanka"},
                {"code":"SH", "country": "St. Helena"},
                {"code":"PM", "country": "St. Pierre and Miquelon"},
                {"code":"SD", "country": "Sudan"},
                {"code":"SR", "country": "Suriname"},
                {"code":"SJ", "country": "Svalbard and Jan Mayen Islands"},
                {"code":"SZ", "country": "Swaziland"},
                {"code":"SE", "country": "Sweden"},
                {"code":"CH", "country": "Switzerland"},
                {"code":"SY", "country": "Syria"},
                {"code":"TW", "country": "Taiwan"},
                {"code":"TJ", "country": "Tajikistan"},
                {"code":"TZ", "country": "Tanzania"},
                {"code":"TH", "country": "Thailand"},
                {"code":"TG", "country": "Togo"},
                {"code":"TK", "country": "Tokelau"},
                {"code":"TO", "country": "Tonga"},
                {"code":"TT", "country": "Trinidad and Tobago"},
                {"code":"TN", "country": "Tunisia"},
                {"code":"TR", "country": "Turkey"},
                {"code":"TM", "country": "Turkmenistan"},
                {"code":"TC", "country": "Turks and Caicos Islands"},
                {"code":"TV", "country": "Tuvalu"},
                {"code":"UG", "country": "Uganda"},
                {"code":"UA", "country": "Ukraine"},
                {"code":"AE", "country": "United Arab Emirates"},
                {"code":"UK", "country": "United Kingdom"},
                {"code":"US", "country": "United States"},
                {"code":"UM", "country": "US Minor Outlying Islands"},
                {"code":"UY", "country": "Uruguay"},
                {"code":"UZ", "country": "Uzbekistan"},
                {"code":"VU", "country": "Vanuatu"},
                {"code":"VA", "country": "Vatican City State (Holy See)"},
                {"code":"VE", "country": "Venezuela"},
                {"code":"VN", "country": "Viet Nam"},
                {"code":"VG", "country": "Virgin Islands (British)"},
                {"code":"VI", "country": "Virgin Islands (US)"},
                {"code":"WF", "country": "Wallis and Futuna Islands"},
                {"code":"EH", "country": "Western Sahara"},
                {"code":"YE", "country": "Yemen"},
                {"code":"YU", "country": "Yugoslavia"},
                {"code":"ZR", "country": "Zaire"},
                {"code":"ZM", "country": "Zambia"},
                {"code":"ZW", "country": "Zimbabwe" }
                ]
                };

        }


    });

});