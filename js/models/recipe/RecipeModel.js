define([
    'underscore',
    'backbone',
    'collections/recipeIngredients/RecipeIngredientsCollection',
    'models/step/IngredientStep'


], function (
    _,
    Backbone,
    RecipeIngredients,
    IngredientStep
) {

    "use strict";

    return Backbone.Model.extend({
    	
    	defaults: {
        },

        initialize: function () {
            this.recipeIngredients = new RecipeIngredients();
        },

        makeFromServerRecipeAndIngredients: function(serverRecipeObject,ingredientsCollection){
            this.clear();
            console.log("Making recipe from server recipe");
            //console.log(serverRecipeObject);
            //console.log(ingredientsCollection);

            this.recipeIngredients = new RecipeIngredients();

            var serverRecipeIngredients = serverRecipeObject.recipeIngredients.models;
            
            console.log("ingredients collection:");
            console.log(ingredientsCollection);

            var that = this;
            $(serverRecipeIngredients).each(function(index, item){
                //console.log("!!!!!!!!");
                var ingredientId = item.get("ingredientId");
                ingredientId = parseInt(ingredientId);
                var qty = item.get("qty");
                var type = item.get("type");
                type = parseInt(type)
                console.log("ingredient id:");
                console.log(ingredientId);
                var ingredient = ingredientsCollection.getIngredient(ingredientId);

                if(ingredient)
                {
                    var ingredientName = ingredient.get("ingredientName");
                    /*console.log("id: "+ingredientId);
                    console.log("qty: "+qty);
                    console.log("type: "+type);
                    console.log("name: "+ingredientName);*/

                    var ingredientStep = new IngredientStep();
                    ingredientStep.set("ingredientId",ingredientId);
                    ingredientStep.set("qty",qty);
                    ingredientStep.set("type",type);
                    ingredientStep.set("ingredientName",ingredientName);
                    ingredientStep.calculateVolume();

                    //console.log("volume: "+ingredientStep.get("volume"));

                    that.recipeIngredients.push(ingredientStep);
                }
                

            });

            this.set("alcContent",serverRecipeObject.get("alcContent"));
            this.set("drinkName",serverRecipeObject.get("drinkName"));
            this.set("userName",serverRecipeObject.get("userName"));
            this.set("recipeId",serverRecipeObject.get("recipeId"));

        },

        //Manually serialize object (gives us flexibility when API changes)
        serialize: function() {
            var object = {};
            object.drinkName = this.get("name");
            object.recipeId  = this.get("recipeId");

            object.email    = this.get("email");
            object.password = this.get("password");

            //Important --> MAKE SURE TO SLICE A NEW ARRAY
            var ingredientsModels = this.recipeIngredients.models.slice(); //Get a new array!

            var actionModel = this.actionIngredient;

            if(actionModel)
            {
                actionModel.set("qty",1);
                ingredientsModels.push(actionModel);
            }

            var strainModel = this.strainIngredient;
            if(strainModel)
            {
                strainModel.set("qty",1);
                ingredientsModels.push(strainModel);
            }

            var payloadIngredientsList = _.map(ingredientsModels,function( model ){
                var payloadObject           = {};
                payloadObject.ingredientId  = model.get("ingredientId");
                //payloadObject.qty         = model.get("volume");
                payloadObject.qty           = model.get("qty");
                return payloadObject;
            });

            object.ingredientList = payloadIngredientsList;
            return JSON.stringify(object);
        },

        canIncreaseIngredient : function (ingredientType, ingredientId) {
            // Get current total volume.
            var totalVolume = _.reduce(this.recipeIngredients.models, function(memo, item) {
                return memo + item.get('qty');
            }, 0);

            // nlam
            var increment;
            if (ingredientId == 46 || ingredientId == 47) {
                increment = 50;
            } else {
                increment = App.conversionMapping[ingredientType] * App.incrementVolumeMapping[ingredientType];
            }

            return !(parseFloat(totalVolume + increment) > 200.0)
        },

        isBelowAlcoholLimit : function (ingredientType) {
            // Get current total volume of alcohol
            var limit = 88,
                increment = 10,
                alcoholVolume = _.reduce(this.recipeIngredients.models, function(memo, item) {
                if(item.get('type') == 1 || item.get('type') == 2) {
                    return memo + item.get('qty');
                } else {
                    return memo;
                }
            }, 0);

            if(ingredientType == 1 || ingredientType == 2) {
                alcoholVolume += increment;
            }

            return (alcoholVolume <= limit);
        },

        totalVolume : function() {
            return _.reduce(this.recipeIngredients.models, function(memo, item) {
                return memo + item.get('qty');
            }, 0);
        }



    });

 });