define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'text!templates/common/inDevelopmentTemplate.html'
], function ($, jquerymobile, _, Handlebars, Backbone, InDevelopmentTemplate) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {
        },

        render: function (eventName) {
            var template  = Handlebars.compile(InDevelopmentTemplate);
            this.$el.html(template); 
        }
    });

});