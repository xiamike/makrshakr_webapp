define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'text!templates/design/ingredientsListTemplate.html'
], function (
  $,
  jquerymobile,
  _,
  Handlebars,
  Backbone,
  IngredientsListTemplate
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function () {
            _.bindAll(this);

            Handlebars.registerHelper("ifIngredient", function (item, block) {
                return this.type != App.actionsIndex ? block.fn(this) : block.inverse(this);
            });

            Handlebars.registerHelper("cost", function (item, block) {
                var  price = parseFloat(this.price),
                    volume = parseFloat(this.volume),
                      cost = price*volume;

                return cost;
            });

            //listen to collection changes
            this.listenTo(this.model.recipeIngredients, 'change', this.render);
            this.listenTo(this.model.recipeIngredients, 'add', this.render);
            this.listenTo(this.model.recipeIngredients, 'add', this.scrollTop);
            this.listenTo(this.model.recipeIngredients, 'remove', this.render);
        },

        scrollTop: function() {
            //scroll to bottom
            $("body, html").animate({
              scrollTop: $(".ui-page").prop("scrollHeight")
            }, 
            400);
        },

        render: function () {
          console.log("IngredientsListView:render");
            var template  = Handlebars.compile(IngredientsListTemplate);

            _.each(this.model.recipeIngredients.models, function(ingredient) {
              ingredient.attributes.ingredientName = ingredient.attributes.ingredientName.replace('~', ' ');
            });

            var steps = this.model.recipeIngredients.toJSON();
            var el = template({steps: steps});
            this.$el.html(el);
            this.$el.trigger("create");
            
            var that = this;

        /*----- Resize list items based on volume -----*/
            this.$(".added-ingredient").each(function(index,element){
              var ingredientIndex = $(element).data("step-number");
              ingredientIndex = parseInt(ingredientIndex);

            //Prevent bad array access
              if (ingredientIndex >= 0
                  && ingredientIndex < that.model.recipeIngredients.length
                  && !isNaN(ingredientIndex)
              ) {

            //Size element according to volume
                var ingredient = that.model.recipeIngredients.at(ingredientIndex);
                var volume = ingredient.get("volume");
                var type = ingredient.get('type');
                var incrementUnit = App.initVolumeMapping[type];
                var height = 0 + parseFloat(volume / incrementUnit) * 44;
                $(element).height(height);

            //Prevent user from changing ice units
                var ingredientId = ingredient.get('ingredientId');
                ingredientId = parseInt(ingredientId);
                if(ingredientId==App.iceId)
                {
                  $(element).find(".ingredient-controls").hide();
                }


              }
            });
        },

        






    });

});