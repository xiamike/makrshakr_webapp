define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'views/common/NavigationHeaderView',
  'views/common/NavigationFooterView',
  'text!templates/order/drinkViewTemplate.html',
  'views/design/WaveGraphicView',
  'views/design/ReviewListView',
  'views/design/DrinkProgressView',
  'text!templates/design/staticActionStepTemplate.html',
  'models/recipe/RecipeModel',
  'collections/recipeIngredients/RecipeIngredientsCollection'

], function (
  $,
  jquerymobile,
  _,
  Handlebars,
  Backbone,
  NavigationHeaderView,
  NavigationFooterView,
  DrinkViewTemplate,
  WaveGraphicView,
  ReviewListView,
  DrinkProgressView,
  StaticActionStepTemplate,
  Recipe,
  RecipeIngredients
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {

          _.bindAll(this);

          this.serverRecipe = options.serverRecipe;
          this.recipe = new Recipe(this.serverRecipe.attributes);
          this.recipe.recipeIngredients = this.serverRecipe.recipeIngredients;
          console.log(this.recipe);
          //window.addEventListener('resize', this.resizeAndRenderWave, false);

        },

        render: function (event) {
          console.log("Render success create view");
            var template  = Handlebars.compile(DrinkViewTemplate);
            
            var alcohol = this.recipe.get("alcContent");
            alcohol = parseFloat(alcohol);
            alcohol = Math.round(alcohol.toFixed(2)*100);

            
            console.log(this.recipe.get("drinkName"));

            var data = {
              drinkName:    this.recipe.get("drinkName"),
              alcoholPercentage:      alcohol,
              userName:     this.recipe.get("userName")
            };

            var html = template(data);
            this.$el.html(html);

            this.loadSubViews();

            return this;
        },

        events: {
            "click      #nextButton"             :"order",
            "click      #customizeButton"        :"customize"
        },

        order: function() {
          console.log("DrinkView >> dispatching order request:");
          console.log(this.recipe);

          // NLAM - 

          /*
          window.EventDispatcher.trigger("orderServerRecipe",this.recipe);
          */

          event.preventDefault(); //prevent linking
        },

        customize: function(){
          console.log("Should allow drink customization");
          event.preventDefault(); //prevent linking
          App.recipe = new Recipe()
          App.recipe.recipeIngredients = this.recipe.recipeIngredients;
          window.EventDispatcher.trigger("customizeRecipe");
        },

        /*----- Wave height logic -----*/
        resizeAndRenderWave: function() {
            var ingredients = this.recipe.recipeIngredients.getIngredientsOnly();
            //var numIngredients = ingredients.length;

            //Calculate height of ingredient elements
            //Make sure to account for border heights
            var totalHeight = 1;
            
            $(ingredients).each(function(index,element){
                var volume = element.get("volume");
                var type = element.get('type');
                var incrementUnit = App.initVolumeMapping[type];

                // nlam
                var height = 0 + parseFloat(volume / incrementUnit) * (44) + 1;
                // height = parseInt(height / 44) * 44 +1;
                // console.log(">>>" + height);

                totalHeight+=height;
            });
            var height = totalHeight;

            //Important!
            this.$("#waveCanvas")[0].width = this.$("#wave-wrapper").width()-1;
            //console.log(this.$("#waveCanvas").width());
            //console.log(this.$("#wave-wrapper").width());


            this.waveGraphicView.renderWave(height-8);
        },

        postShowSetup: function()
        {
            //this.resizeAndRenderWave();
        },

        loadSubViews: function() {
          console.log("Create success view load subviews");

            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Drink results"
            });
            this.navigationHeaderView.render();

            this.reviewListView = new ReviewListView({
                el: this.$("#recipe-steps-list-wrapper"),
                model: this.recipe
            });
            this.reviewListView.render();


            this.drinkProgressView = new DrinkProgressView({
                el: this.$("#drink-progress-wrapper"),
                model: this.recipe
            });
            this.drinkProgressView.render();
            this.drinkProgressView.animate();

            //WaveGraphicView
            //this.waveGraphicView = new WaveGraphicView({
            //    el: this.$("#wave-wrapper")
            //});

            //this.waveGraphicView.render();
            //this.resizeAndRenderWave();

            //Append action and strain by hand
            this.$("#recipe-actions-list-wrapper").empty();

            if(this.recipe.actionIngredient||this.recipe.strainIngredient)
            {
                this.$("#recipe-actions-list-wrapper").empty();
                this.$("#recipe-actions-list-wrapper").append("<div class='small-instructions-2'><p>Preparation method:</p></div>");
                this.$("#recipe-actions-list-wrapper").append("<ul id='action-steps-list' data-role='listview' data-inset='true' data-corners='false'></ul>");

                if(this.recipe.actionIngredient)
                {
                    var template  = Handlebars.compile(StaticActionStepTemplate);
                    var data = this.recipe.actionIngredient.toJSON();
                    var html = template(data);
                    this.$("#action-steps-list").append(html);
                }

                if(this.recipe.strainIngredient)
                {
                    var strainTemplate  = Handlebars.compile(StaticActionStepTemplate);
                    var strainData = this.recipe.strainIngredient.toJSON();
                    var strainHtml = strainTemplate(strainData);
                    this.$("#action-steps-list").append(strainHtml);
                }


            }

            ////////////////////////////
            /* Footer navigation here */
            ////////////////////////////

            /*this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    true,
                nextButtonTitle:    "Order this drink",
                nextAllowed:        true,
                nextHref:           "#order" 
            });
            this.navigationFooterView.render();*/

            ///////////////////////////
            /* End footer navigation */
            ///////////////////////////


            //this.NextButtonView.render();
        }

    });

});