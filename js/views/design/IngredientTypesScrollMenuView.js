define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/design/ingredientTypesScrollMenuTemplate.html'
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    IngredientTypesScrollMenuTemplate
) {

    "use strict";

    //var scrollView;

    return Backbone.View.extend({

        initialize: function() {
            this.render();
        },

        render: function () {
            var el = Handlebars.compile(IngredientTypesScrollMenuTemplate),
            
            scrollViewHTML = el(this.options.data);

            this.$el.html(scrollViewHTML);

            this.resizeScroller();
            this.initScroller();

            this.scrollScroller();
        },

        resizeScroller: function() {
            var scrollEl = this.$("#types-scroll-view ul li");
            var numItems = this.options.data.items.length;
            var width = 151; //Hardcoding
            var margin = 0;
            //var width = parseInt(scrollEl.css("width"));
            //var margin = parseInt(scrollEl.css("margin-left"));
            var totalWidth = numItems * width + (numItems+1) * margin;

            this.$("#types-scroll-view").width(totalWidth);
            this.$("#types-scroll-view ul").width(totalWidth);
        },

        initScroller: function() {
            var s = this.$("#types-scroll-wrapper")[0];
            App.typesScrollView = new iScroll(s, {
                hScrollbar      : false, 
                vScrollbar      : false, 
                fixedScrollbar  : true
            });

            //use vmousemove for testing (captures both mouse and touch moves)
            $(document).bind('vmousemove', function (e) {
                //console.log(e.target.id);
                if (e.target.id == "types-scroll-wrapper") {
                    e.preventDefault();
                }
            });

            var that = this;
            setTimeout(function () { App.typesScrollView.refresh(); }, 0);

        },

        scrollScroller: function() {
            $('#types-scroll-wrapper')
                .delay('1000')
                .scrollTo('25px', {duration: 500, axis: 'x'})
                .scrollTo('0px', {duration: 100, axis: 'x'});
        }

    });

});