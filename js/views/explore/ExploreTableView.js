define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'collections/ingredients/IngredientsCollection',
  'collections/recipeIngredients/RecipeIngredientsCollection',
  'models/recipe/ServerRecipeModel',
  'models/recipe/RecipeModel',
  'views/suggestions/DrinkListItemView',
  'text!templates/explore/exploreTableTemplate.html'
], function (
  $,
  jquerymobile,
  _,
  Handlebars,
  Backbone,
  IngredientsCollection,
  RecipeIngredients,
  ServerRecipe,
  Recipe,
  DrinkListItemView,
  ExploreTableTemplate) {

    "use strict";

    return Backbone.View.extend({

        /*----- Events -----*/
        events: {
            "click      .explore-drink"              :"tappedDrink",
        },

        tappedDrink: function (event) {

            //only allow if we have ingredients!

            //Get recipeId
            var recipeId = $(event.currentTarget).data("recipe-id");
            recipeId = parseInt(recipeId);
            recipeId = recipeId.toString()
            console.log("tapped recipe with id: "+recipeId);

            var recipe = new ServerRecipe();

            var data = JSON.stringify({"recipeId": recipeId});
            var that = this;
            $.post("proxy/kukaproxy.php?path=getRecipes", data)

                //Success callback
                .done(function(data, textStatus, jqXHR)
                {
                        var code = data.code;

                        //Validate server response for true success
                        if( typeof(code) !== 'undefined'
                          && parseInt(code) == 1)
                        {
                          //console.log(data);

                          //FIXME: Very bad practice on server. Returns array on multiple objects and object on single object response
                          var serverRecipe = new ServerRecipe(data.recipeList);
                          //Hack to fix above
                          serverRecipe.recipeIngredients = new RecipeIngredients(data.recipeList.ingredientList);
                          console.log(serverRecipe);
                          //Hide loading UI
                          $.mobile.loading("hide");


                          //Hack: Need ingredients to create recipe. Server does not return!

                          /*!!!!!!!!!!!!!!!!!!!!!!!!!!*/
                          that.ingredients.fetch({
                              //Handle success
                              success: function(data, textStatus, jqXHR)
                              {
                                  console.log(textStatus);
                                  //Validate true success
                                  if(textStatus
                                      && parseInt(textStatus.code)==1
                                      && textStatus.ingList.length>0)
                                  {
                                     
                                    var localRecipe = new Recipe();
                                    console.log(that.ingredients);
                                    localRecipe.makeFromServerRecipeAndIngredients(serverRecipe, that.ingredients);
                                    console.log(localRecipe);
                                    window.EventDispatcher.trigger("showServerRecipe", localRecipe);
                             
                              /*****************************************/

                                  } else
                                  {
                                      //Dispatch error event
                                      window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                                  }

                              },

                              //Handle error
                              error: function( jqXHR, textStatus, errorThrown)
                              {
                                  //Dispatch error event
                                  window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                              }

                          });
                          /*!!!!!!!!!!!!!!!!!!!!!!!!!!*/







                        }
                        else //Received a failure response from server
                        {
                          console.log("Failed to load recipe.");
                          
                          //Error message from server:
                          var message = data.message;

                          that.showErrorPopUp("Failed to load recipe: "
                            +message);

                          //TODO: fully handle failure
                        }
                      })

                //Failure callback
                      .fail(function(jqXHR, textStatus, errorThrown)
                      {
                        console.log("Failed to load recipe: "+textStatus);

                        //that.navigate('review', {trigger: false});
                        that.showErrorPopUp(exception);
                        //TODO: fully handle failure
                    });



            /*


            var results = this.serverRecipes.where({recipeId: recipeId});
            var serverRecipe;
            if (results.length>0)
            {
                serverRecipe = results[0];
            }
            //console.log(serverRecipe);
            
            console.log("server recipe is: ");
            console.log(serverRecipe);

            var recipe = new Recipe();
            recipe.makeFromServerRecipeAndIngredients(serverRecipe,this.ingredients);
            console.log("recipe is: ");
            console.log(recipe);
            
            window.EventDispatcher.trigger("showServerRecipe", recipe);
            window.EventDispatcher.trigger("showServerRecipeFromProfile", recipe);*/





            event.preventDefault(); //Prevent linking
        },

        initialize: function (options) {
          this.drinks = options.drinks;
          this.ingredients = new IngredientsCollection();
          console.log(this.drinks);

           Handlebars.registerHelper("displayAlcoholContent", function (item, block) {
                var percentage = parseFloat(this.alcContent);
                percentage = parseFloat(percentage);
                percentage = Math.round(percentage.toFixed(2)*100);
                return percentage
            });

        },

        render: function (eventName) {
            var template  = Handlebars.compile(ExploreTableTemplate);

            var el = template({drinks: this.drinks});
            this.$el.html(el); 

            /*$.each(this.drinks,function(index,value){

              var itemView = new DrinkListItemView({
                //el: that.$("#drinks-list"),
                model: value,
              });
              var itemView = itemView.render();
              that.$("#drinks-list").append(itemView.el);
            });*/


        }
    });

});