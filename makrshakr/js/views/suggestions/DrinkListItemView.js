define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'text!templates/suggestions/drinkListItemTemplate.html',
  'views/common/RecipeGraphicView'
], function (
  $,
  jquerymobile,
  _,
  Handlebars,
  Backbone,
  DrinkListItemTemplate,
  RecipeGraphicView
) {

    "use strict";

    return Backbone.View.extend({
        tagName:    "li",
        className:  "drink",
//<li class="drink" recipe-id="{{this.recipeId}}" data-icon="false">


        initialize: function () {
            _.bindAll(this);
            //console.log("DrinkListItemView init");
            //console.log("model is:");
            //console.log(this.model);
        },

        render: function () {
            //console.log("DrinkListItemView render");

            //console.log(this.$el);
            this.$el.attr("data-recipe-id",this.model.get("recipeId"));
            this.$el.attr("data-icon",false);
            this.$el.attr("href","test");

            var template  = Handlebars.compile(DrinkListItemTemplate);
            var data = this.model.toJSON();

            //convert alcohol to more human readable format
            var alcohol = this.model.get("alcContent");
            alcohol = parseFloat(alcohol);
            alcohol = Math.round(alcohol.toFixed(2)*100);
            //console.log(alcohol);
            data.alcoholPercentage = alcohol;
            //console.log(data);
            var el = template(data);
            this.$el.html(el); //important to append
            this.loadSubViews();
            return this;

        },

        postRenderSetup: function () {
        },

        loadSubViews: function() {
          //console.log("DrinkListItemView loadsubviews");
          //console.log(this.$("#recipe-graphic-wrapper").length);
            
            this.recipeGraphicView = new RecipeGraphicView({
                el: this.$("#recipe-graphic-wrapper"),
                model: this.model
            })

            this.recipeGraphicView.render();
            
            this.recipeGraphicView.renderGraphic(80,80,30,false);

        }
      });

});