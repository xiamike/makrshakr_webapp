define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/common/popUpTemplate.html',
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    popUpTemplate
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {
            _.bindAll(this);
            this.message = options.message;
        },

        render: function () {
            console.log("Render popup");
            var template  = Handlebars.compile(popUpTemplate);
            var data = {message: this.message};
            var html = template(data);

            this.$el.html(html);
        },

        postRenderSetup: function() {
          
        },

    });

});