define([
    'jquery',
    'underscore',
    'backbone',
    'd3',
    'text!templates/explore/exploreVisualizationTemplate.html'
], function ($, _, Backbone, D3, ExploreVisualizationTemplate) {

    "use strict";

    return Backbone.View.extend({
        el: "#visualization-wrapper",


        events: {
            "vmousedown #visualization-canvas"  : "tappedVis",
            "click path"                        : "test"
        },

        test: function () {
            console.log("test");
        },
        
        tappedVis: function () {
            console.log("Tapped Vis");

            //Div offset
            var docX = $("#visualization-wrapper").offset().left;
            var docY = $("#visualization-wrapper").offset().top;

            //Location on page
            var posX = event.pageX;
            var posY = event.pageY;

            //Location in visualization
            var visX = posX - docX;
            var visY = posY - docY;

            var lookupPoint = [visX, visY];
            var results = this.getNearbyDrinks(this.radius * 3, lookupPoint);
            console.log(results);

            // Dispatch tap event (ask Router to show a tableview with nearby drinks).
            window.EventDispatcher.trigger("exploreVisualization:zoom", results);
        },

        graphAgePopularity : function () {
            this.graphType = 1;
            this.renderVisualization(this.drinksJson);
        },

        graphAgeAlcohol : function () {
            this.graphType = 2;
            this.renderVisualization(this.drinksJson);
        },

        graphPopularityAlcohol : function () {
            this.graphType = 3;
            this.renderVisualization(this.drinksJson);
        },

        //Get drinks data near tap/click
        getNearbyDrinks: function (lookupRadius, lookupPoint) {

            // Iterate through drinks data, saving points.
            // Within lookupRadius distance of lookupPoint.
            var results = [];
            for (var i = 0; i < this.drinksJson.length; i++) {

                /*
                var alcoholValue = parseFloat(this.drinksJson[i]["alcContent"]);
                var ageValue     = parseFloat(this.drinksJson[i]["age"]);
                var x            = this.alcoholScale(alcoholValue);
                var y            = this.ageScale(ageValue);
                */

                // TODO: Refactor
                var x, y;

                if (this.graphType == 1) {
                    x = this.ageScale(this.drinksJson[i]["age"]);
                    y = this.popularityScale(this.drinksJson[i]["popularity"]);
                } else if (this.graphType == 2) {
                    x = this.ageScale(this.drinksJson[i]["age"]);
                    y = this.alcoholScale(this.drinksJson[i]["alcContent"]);
                } else if (this.graphType == 3) {
                    x = this.popularityScale(this.drinksJson[i]["popularity"]);
                    y = this.alcoholScale(this.drinksJson[i]["alcContent"]);
                } 

                var xDist        = lookupPoint[0] - x;
                var yDist        = lookupPoint[1] - y;

                var distance = Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));

                if (distance < lookupRadius) {
                    results.push(this.drinksJson[i]);
                }
            }
            return results;
        },


        initialize: function () {

            // Wake up with Age vs Popularity
            this.graphType = 1;
        },

        render: function (eventName) {
            console.log("render ExploreVisualizationView");

            var template  = Handlebars.compile(ExploreVisualizationTemplate);
            var el = template( { drinks: null } );
            this.$el.children("#graph").append(el);
            //this.setElement(el);
            //$("#visualization-wrapper").replaceWith(el);
            //$("#visualization-wrapper").trigger("create");
            //this.renderVisualization(this.collection);
        },

        resizeWindow: function () {
            //console.log("window resizing");
        },

        getScreenRange: function (radius) {
            var width = $("#visualization-wrapper").width();
            width = parseFloat(width);
            return [radius*2, width-radius*2];
        },

        //Generate array of hexagon points given center X and Y and a radius
        hexagonPoints: function (centerX, centerY, radius) {
            var angleStep = 2*Math.PI/6;
            var points = [];

            for(var i = 0; i < 6; i++) {
                var x = centerX + Math.cos(angleStep * i+angleStep/2) * radius;
                var y = centerY + Math.sin(angleStep * i+angleStep/2) * radius;
                var point = [];
                point["x"] = x;
                point["y"] = y;
                points[i]=point;
            }
            
            return points;
        },

        d3Line: function () {
             d3.svg.line()
                .x(function(d){
                    return d.x;
                })
                .y(function(d){
                    return d.y;
                })
                .interpolate("linear");
        },

        getValueRange: function (drinksJson, attributeKey, flipAxis) {
            
            if (typeof(flipAxis) ==='undefined') {
                flipAxis = false;
            }

            var minValue = Number.MAX_VALUE;
            var maxValue = 0;
      
            for (var i = 0; i < drinksJson.length; i++) {
                var tempValue
                tempValue = parseFloat(drinksJson[i][attributeKey]);
               
                maxValue = Math.max(tempValue, maxValue);
                minValue = Math.min(tempValue, minValue);
            }

            if (flipAxis==true) {
                console.log("Range:" + [maxValue,minValue]);
                return [maxValue, minValue];
            } else {
                console.log("Range:" + [minValue, maxValue]);
                return [minValue, maxValue];
            }

        },

        getMinValue: function (drinksJson, attributeKey) {

            var minValue = Number.MAX_VALUE;

            for (var i = 0; i < drinksJson.length; i++) {
                var tempValue
                tempValue = parseFloat(drinksJson[i][attributeKey]);
               
                minValue = Math.min(tempValue, minValue);
            }
          
            return minValue;
        },


        getAndSaveRanges: function (drinksJson) {
            this.screenRange = this.getScreenRange(this.radius);

            this.alcoholRange    = this.getValueRange(drinksJson, "alcContent");
            this.ageRange        = this.getValueRange(drinksJson, "age", true);
            this.popularityRange = this.getValueRange(drinksJson, "popularity");

            this.genderRange     = [0 , 4];
        
            this.alcoholScale     = d3.scale.linear().domain(this.alcoholRange).range(this.screenRange);
            this.ageScale         = d3.scale.linear().domain(this.ageRange).range(this.screenRange);
            this.popularityScale  = d3.scale.linear().domain(this.popularityRange).range(this.screenRange);
            this.genderScale      = d3.scale.linear().domain(this.genderRange).range(this.screenRange);
        },


        renderVisualization: function (drinksJson) {

            this.drinksJson = drinksJson;

            console.log("rendering visualization");
            console.log(drinksJson);

            // Clear drawing area.
            $("#visualization-canvas").empty();

            var drawContainer = d3.select("#visualization-canvas");

            //SB: HACK
            //For some reason DOM element is not expanding to 100% width here
            //when navigating using browser back and forward
            
            var pageWidth = parseFloat($("body").width());

            // var pageWidth = 500.0;
            var padding    = parseFloat($(".ui-content").css("padding-left"));

            //padding = padding + parseFloat($(".ui-content").css("padding-right"));
            console.log(padding);
            padding = 20;
            $("#visualization-wrapper").width(pageWidth-padding * 2);

            var width  = $("#visualization-wrapper").width();
            var height = $("#visualization-wrapper").width();
            

            // Set hexagon width relative to screen width.
            var numDataPointsPerWidth = 20;
            var length  = width / numDataPointsPerWidth;
            this.radius = length / 2; //save radius so we can use on calculations later


            this.getAndSaveRanges(this.drinksJson);

            // Container for drawing.
            var svgContainer = drawContainer.append("svg")
                .attr("width",width)
                .attr("height",height);


            //Draw axis
            /*
            var yAxis = svgContainer.append("line")
                .attr("x1",width/2)
                .attr("y1",this.radius)
                .attr("x2",width/2)
                .attr("y2",height-this.radius)
                .style("stroke","#000")
                .style("stroke-width", "1");

            var xAxis = svgContainer.append("line")
                .attr("x1",this.radius)
                .attr("y1",height/2)
                .attr("x2",width-this.radius)
                .attr("y2",height/2)
                .style("stroke","#000")
                .style("stroke-width", "1");
            */

            // Helper function for constructing lines
            var lineFunction = d3.svg.line()
                .x(function(d) { return d.x; })
                .y(function(d) { return d.y; })
                .interpolate("linear");

            // Set pattern for hexagons
            var defs = svgContainer.append('svg:defs');
            
            var gridPattern = defs.append('pattern')
                .attr('id','grid')
                .attr('patternUnits', 'userSpaceOnUse')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  326)
                    .attr('height', 266)
                    .append('image')
                        .attr('xlink:href','images/vis_background_grid2.png')
                        .attr('x',0)
                        .attr('y',0)
                        .attr('width',  326)
                        .attr('height', 266);


            var bluePattern = defs.append('pattern')
                .attr('id','blue-stripe')
                    .attr('patternUnits', 'userSpaceOnUse')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  5)
                    .attr('height', 5)
                    .append('image')
                        .attr('xlink:href','images/blue_stripe.png')
                        .attr('x',0)
                        .attr('y',0)
                        .attr('width',  5)
                        .attr('height', 5);

            /*var bluePattern = defs.append('pattern')
                .attr('id','blue-stripe')
                .attr('patternUnits', 'objectBoundingBox')
                .attr('patternContentUnits','objectBoundingBox')
                .attr('x',0)
                .attr('y',0)
                .attr('width',  3)
                .attr('height', 3)
                .append("path")
                    .attr("d","M0 0.5 L 0.5 0 M0 0.8 L 0.8 0 M0 1.1 L 1.1 0 M0 1.4 L 1.4 0")
                    .style("stroke","blue")
                    .style("stroke-width", "0.01")*/

            var redPattern = defs.append('pattern')
                .attr('id','red-stripe')
                    .attr('patternUnits', 'userSpaceOnUse')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  5)
                    .attr('height', 5)
                    .append('image')
                        .attr('xlink:href','images/red_stripe.png')
                        .attr('x',0)
                        .attr('y',0)
                        .attr('width',  5)
                        .attr('height', 5);
            /*var redPattern = defs.append('pattern')
                .attr('id','red-stripe')
                .attr('patternUnits', 'objectBoundingBox')
                .attr('patternContentUnits','objectBoundingBox')
                .attr('x',0)
                .attr('y',0)
                .attr('width',  10)
                .attr('height', 10)

                .append("path")
                    .attr("d","M0 0.5 L 0.5 0 M0 0.8 L 0.8 0 M0 1.1 L 1.1 0 M0 1.4 L 1.4 0")
                    .style("stroke","red")
                    .style("stroke-width", "0.01")*/

            var grayPattern = defs.append('pattern')
                .attr('id','grey-stripe')
                    .attr('patternUnits', 'userSpaceOnUse')
                    .attr('x',0)
                    .attr('y',0)
                    .attr('width',  5)
                    .attr('height', 5)
                    .append('image')
                        .attr('xlink:href','images/grey_stripe.png')
                        .attr('x',0)
                        .attr('y',0)
                        .attr('width',  5)
                        .attr('height', 5);

            /*var grayPattern = defs.append('pattern')
                .attr('id','gray-stripe')
                .attr('patternUnits', 'userSpaceOnUse')
                .attr('x',0)
                .attr('y',0)
                .attr('width',  3)
                .attr('height', 3)

                .append("line")
                    .attr("x1",0)
                    .attr("y1",3)
                    .attr("x2",3)
                    .attr("y2",0)
                    .style("stroke","gray")
                    .style("stroke-width", "1")
                    .style("stroke-linecap", "square");*/

             // Background layer.
            var backgroundLayer = svgContainer.append("rect")
                .attr("width",width)
                .attr("height",height)
                .attr("x",0)
                .attr("y",0)
                .style("fill","white");
                //.style("stroke-width","4")
                //.style("stroke","#000");

            var gridLayer = svgContainer.append("rect")
                .attr("width",width)
                .attr("height",height)
                .attr("x",0)
                .attr("y",0)
                .style("fill","url('#grid')");


            // Draw hexagons using joins.
            // http://bost.ocks.org/mike/join/
            var that = this;
            var hexagons = svgContainer.selectAll("path")
            .data(drinksJson)
            .enter().append("path")
            .attr("class","hexagon")
            .attr("d", function(d) {

                // TODO: Refactor
                var x;
                if (that.graphType == 1) {
                    x = that.ageScale(d.age);
                } else if (that.graphType == 2) {
                    x = that.ageScale(d.age);
                } else if (that.graphType == 3) {
                    x = that.popularityScale(d.popularity);
                }

                var y = 0 - that.radius;

                var points = that.hexagonPoints(x, y, that.radius);
                return lineFunction(points)+"Z";
            })

            .style("fill-opacity",0.5)
            .style("fill",function(d){
                if(d.gender=="male")
                {
                    return "url('#blue-stripe')";
                } else if(d.gender=="female")
                {
                    return "url('#red-stripe')";
                } else
                {
                    return "url('#gray-stripe')";
                }
            })
            .style("stroke-opacity",0.8)
            .style("stroke-width",1)
            .style("stroke",function(d){
                if(d.gender=="male")
                {
                    return "blue";
                } else if(d.gender=="female")
                {
                    return "red";
                } else
                {
                    return "gray";
                }
            });

            //hexagons.transition.transition().style("stroke-width", "5");
            var n = drinksJson.length;
            var duration = 1000;
            console.log("length");
            console.log(n);
            svgContainer.selectAll(".hexagon").transition()
                .delay(function(d, i) { return i / n * duration; })
                .attr("d", function(d) {


                // TODO: Refactor
                var x, y;

                if (that.graphType == 1) {
                    x = that.ageScale(d.age);
                    y = that.popularityScale(d.popularity);
                } else if (that.graphType == 2) {
                    x = that.ageScale(d.age);
                    y = that.alcoholScale(d.alcContent);
                } else if (that.graphType == 3) {
                    x = that.popularityScale(d.popularity);
                    y = that.alcoholScale(d.alcContent);
                } 

                var points = that.hexagonPoints(x, y, that.radius);
                 return lineFunction(points)+"Z";
            });

        },

    });

});
