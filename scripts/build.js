({
    appDir: '../js',
    baseUrl: '.',
    dir: '../js-min',

    optimize: 'uglify',
    skipDirOptimize: true,
    normalizeDirDefines: 'skip',

    paths: {
        jquery:         'libs/jquery/jquery',
        jquerymobile:   'libs/jquery.mobile/jquery.mobile',
        jqmconfig:      'libs/jquery.mobile/jqm-config',
        iscroll:        'libs/iScroll/iscroll',
        d3:             'libs/d3/d3.v3.min',
        paper:          'libs/paperjs/paper',
        handlebars:     'libs/handlebars/handlebars',
        underscore:     'libs/underscore/underscore',
        backbone:       'libs/backbone/backbone',
        templates:      '../templates'
    },

    shim: {
        'backbone': {
              'deps': [ 'underscore', 'jquery' ],
              'exports': 'Backbone'  //attaches "Backbone" to the window object
        },

        'handlebars': {
            'exports': 'Handlebars' //attaches "Handlebars" to the window object
        },

        'jqmconfig': {
            'deps': [ 'jquery'],
            'exports': 'JQMConfig'
        },

        'iscroll': {
        },

        'd3': {

        },

        'paper': {

        }
    },

    modules: [
        {
            name: 'mobile'
        }
    ]
})