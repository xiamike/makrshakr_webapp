define([
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'text!templates/design/stepsCompositeTemplate.html',
    'models/step/IngredientStep',
    'models/step/Step',
    'models/recipe/RecipeModel',
    'views/design/StepsListView',
    'views/common/NavigationHeaderView',
    'views/common/NavigationFooterView',
    'views/design/WaveGraphicView'
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    StepsCompositeTemplate,
    IngredientStep,
    Step,
    Recipe,
    StepsListView,
    NavigationHeaderView,
    NavigationFooterView,
    WaveGraphicView
) {

    "use stict";

    return Backbone.View.extend({
        removeHandler: function () {

            console.log('Kill: ', this);

            this.unbind(); // Unbind all local event bindings
            this.remove(); // Remove view from DOM

            //window.removeEventListener('resize', this.resizeAndRenderWave, false);

        },

         setActionsMenu: function(){
            if(this.model.actionIngredient)
            {
                var that = this;
                this.$(".append-action").each(function(){
                    var ingredientId = $(this).data("ingredient-id");
                    ingredientId = parseInt(ingredientId);

                    var actionId = that.model.actionIngredient.get("ingredientId");
                    actionId = parseInt(actionId);
                    if(ingredientId!=actionId)
                    {
                        $(this).removeClass("selected");
                    } else
                    {
                        $(this).addClass("selected");
                    }
                });

            } else
            {
                //select the none tab
                this.$(".append-action").each(function(){
                    var ingredientId = $(this).data("ingredient-id");
                    ingredientId = parseInt(ingredientId);
                    if(ingredientId==-1)
                    {
                        $(this).addClass("selected");
                    } else
                    {
                         $(this).removeClass("selected");
                    }
                    return;
                });
            }
        },

        setStrainMenu: function() {
            if(this.model.strainIngredient)
            {
                var that = this;
                this.$(".strain-button").each(function(){

                    var strainId = $(this).data("strain-id");
                    strainId = parseInt(strainId);
                    if(strainId==0)
                    {
                        $(this).removeClass("selected");
                    } else
                    {
                        $(this).addClass("selected");
                    }
                });
            } else
            {
                //select the none tab
                this.$(".strain-button").each(function(){
                    var strainId = $(this).data("strain-id");
                    strainId = parseInt(strainId);
                    if(strainId==0)
                    {
                        $(this).addClass("selected");
                    } else
                    {
                         $(this).removeClass("selected");
                    }
                    return;
                });
            }
        },

        loadSubViews: function () {
            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Recipe Steps (2/3)",
                nav: 'design'
            });

            this.navigationHeaderView.render();

            this.recipeStepsListView = new StepsListView({
                el: this.$("#recipe-steps-list-wrapper"),
                model: this.model,
                ingredients: this.ingredients
            });
            this.recipeStepsListView.render();

            //WaveGraphicView
            //this.waveGraphicView = new WaveGraphicView({
            //    el: this.$("#wave-wrapper")
            //});
            //this.waveGraphicView.render();

            ////////////////////////////
            /* Footer navigation here */
            ////////////////////////////
            var nextAllowed = false;
            if (this.model.recipeIngredients.length>0)
            {
                console.log("yes");
                nextAllowed = true;
            }

            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    true,
                nextButtonTitle:    "Next",
                nextAllowed:        nextAllowed,
                nextHref:           "#review"
            });
            this.navigationFooterView.render();
            
            this.setActionsMenu();
            this.setStrainMenu();

            //update navigation footer based on collection
            //this.listenTo(this.model.recipeIngredients, 'change', this.updateNextButton);
            //this.listenTo(this.model.recipeIngredients, 'add', this.updateNextButton);
            //this.listenTo(this.model.recipeIngredients, 'remove', this.updateNextButton);
            ///////////////////////////
            /* End footer navigation */
            ///////////////////////////

        },

        postShowSetup: function()
        {
            //this.resizeAndRenderWave();
        },

        /*----- Wave height logic -----*/
        resizeAndRenderWave: function() {
            var ingredients = this.model.recipeIngredients.getIngredientsOnly();
            var numIngredients = ingredients.length;

            //Calculate height of ingredient elements
            //Make sure to account for border heights
            var totalHeight = 1;
            var that = this;
            $(ingredients).each(function(index,element){
                var volume = element.get("volume");
                var type = element.get('type');
                var incrementUnit = App.initVolumeMapping[type];

                var height = 0 + parseFloat(volume / incrementUnit) * (44)+1;

                totalHeight+=height;
            });
            var height = totalHeight;

            //Resize other elements
            //this.$("#wave-wrapper").height = height-8;

            //Hardcoding actions bar height and instructions for now!
            //var minContentHeight = 55+192+height+8;
            //console.log(this.$("#actions-bar").height());
            //console.log(this.$("#actions-bar").length);

            //this.$("#recipe-steps-actions-wrapper").css("min-height",minContentHeight);


            //Important!
            this.$("#waveCanvas")[0].width = this.$("#wave-wrapper").width()-1;
            console.log(this.$("#waveCanvas").width());
            console.log(this.$("#wave-wrapper").width());
            this.waveGraphicView.renderWave(height-8);
        },

        initialize: function (options) {
            console.log("init StepsCompositeView");
            _.bindAll(this);
            this.$el.on('teardown', this.removeHandler);
            //TODO: validate that we have a collection here

            //console.log(this.collection);

            //Assign ingredients passed from router at initialization
            this.ingredients = options.ingredients;

            //window.addEventListener('resize', this.resizeAndRenderWave, false);
        },


        render: function (eventName) {
            console.log("render StepsCompositeView");
            var template  = Handlebars.compile(StepsCompositeTemplate);

            //var steps = this.model.recipeIngredients.toJSON();

            //Don't show the strain action
            //var actions = this.ingredients.getAllActions();
            var actions = this.ingredients.getAllActionsWithoutStrain();
            var el = template({actions: actions});

            this.$el.html(el);

            this.loadSubViews();


            return this;
        },

        postRenderSetup: function () {
        },

        events: {
            "click        .up-ingredient-step"         :"upIngredientStep",
            "click        .down-ingredient-step"       :"downIngredientStep",
            "click        .append-action"              :"appendAction",
            "click        .strain-button"              :"changeStrain",
        },

        upIngredientStep: function (event) {
            //Important!
            //This function will trigger multiple add/remove events on the collection
            //If listening to changes and re-rendering, should only render as necessary

            console.log("Move ingredient up");
            var currentIndex = $(event.currentTarget).parents("li.added-ingredient").data("step-index");
            currentIndex = parseInt(currentIndex);

            var newIndex = currentIndex;

            if (currentIndex > 0) {
                for (var i=currentIndex-1; i>=0; i--) {
                    var step = this.model.recipeIngredients.at(i);
                    var type = step.get("type");
                    if (type != App.actionsIndex) {
                        newIndex=i;
                        break;
                    }
                }
            }
            //console.log("currentIndex: "+currentIndex+", newIndex: ", newIndex);

            if (newIndex != currentIndex) {
                var currentIngredient = this.model.recipeIngredients.at(currentIndex);
                var replaceIngredient = this.model.recipeIngredients.at(newIndex);
                this.model.recipeIngredients.remove(currentIngredient);
                this.model.recipeIngredients.add(currentIngredient,{at:newIndex});
                this.model.recipeIngredients.remove(replaceIngredient);
                this.model.recipeIngredients.add(replaceIngredient,{at:currentIndex});

                //Manually re-render steps list
                this.recipeStepsListView.render();
            } else
            {
                //Dispatch error event
                window.EventDispatcher.trigger("errorPopUp", "Cannot move step up any further.");
            }
            event.preventDefault(); //prevent linking
        },

        downIngredientStep: function (event) {
            //Important!
            //This function will trigger multiple add/remove events on the collection
            //If listening to changes and re-rendering, should only render as necessary
            //Render recipeStepsListView manually
            console.log("Move ingredient down");

            //Current ingredient index
            var currentIndex = $(event.currentTarget).parents("li.added-ingredient").data("step-index");
            currentIndex = parseInt(currentIndex);

            //Find index of next ingredient (non mix step) in the recipe
            var newIndex = currentIndex;
            if (currentIndex < this.model.recipeIngredients.length-1) {
                for (var i = currentIndex+1; i <= this.model.recipeIngredients.length-1; i++) {
                    var step = this.model.recipeIngredients.at(i);
                    var type = step.get("type");
                    type = parseInt(type);
                    if (type != App.actionsIndex) {
                        newIndex=i;
                        break;
                    }
                }
            }

            //Swap ingredient locations
            if (newIndex != currentIndex) {
                var currentIngredient = this.model.recipeIngredients.at(currentIndex);
                var replaceIngredient = this.model.recipeIngredients.at(newIndex);
                this.model.recipeIngredients.remove(currentIngredient);
                this.model.recipeIngredients.add(currentIngredient,{at:newIndex});
                this.model.recipeIngredients.remove(replaceIngredient);
                this.model.recipeIngredients.add(replaceIngredient,{at:currentIndex});
                //Manually re-render steps list
                this.recipeStepsListView.render();
            } else
            {
                //Dispatch error event
                window.EventDispatcher.trigger("errorPopUp", "Cannot move step down any further.");
            }

            event.preventDefault(); //prevent linking
        },

        appendAction: function (event) {
            console.log("appendAction");
            event.preventDefault(); //prevent linking

            var ingredientID = $(event.currentTarget).data("ingredient-id");
            ingredientID = parseInt(ingredientID);

            //Selected "None"
            if(ingredientID == -1)
            {
                delete this.model.actionIngredient;
            } else
            {
                if(this.model.recipeIngredients.doesContainIngredient(App.iceId)&&ingredientID==App.muddleId)
                {
                    //Dispatch error event
                    window.EventDispatcher.trigger("errorPopUp", "Sorry, cannot muddle a drink that contains ice.");
                    return false;
                }


                var ingredient = this.ingredients.getIngredient(ingredientID);
                var newAction = new IngredientStep({ingredientName: ingredient.get("ingredientName"), ingredientId:ingredient.get("ingredientId"), type:ingredient.get("type")});
                this.model.actionIngredient = newAction;
                //this.model.recipeIngredients.add(newAction,{at: insertionIndex});

            }

            this.setActionsMenu();

            //console.log(this.model.serialize());
        },

        changeStrain: function (event) {
            console.log("changeStrain");
            event.preventDefault(); //prevent linking

            var strainId = $(event.currentTarget).data("strain-id");
            strainId = parseInt(strainId);

            //Selected "None"
            if(strainId == 0)
            {
                delete this.model.strainIngredient;
            } else
            {
                var ingredient = this.ingredients.getIngredient(App.strainId);
                var newAction = new IngredientStep({ingredientName: ingredient.get("ingredientName"), ingredientId:ingredient.get("ingredientId"), type:ingredient.get("type")});
                this.model.strainIngredient = newAction;

            }

            this.setStrainMenu();

            //console.log(this.model.serialize());
        },

    });

});