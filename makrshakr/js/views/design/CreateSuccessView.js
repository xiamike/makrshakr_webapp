define([
  'jquery',
  'jquerymobile',
  'underscore',
  'handlebars',
  'backbone',
  'views/common/NavigationHeaderView',
  'views/common/NavigationFooterView',
  'text!templates/design/createSuccessTemplate.html',
  'views/design/WaveGraphicView',
  'views/design/ReviewListView',
  'views/design/DrinkProgressView',
  'text!templates/design/staticActionStepTemplate.html'

], function (
  $,
  jquerymobile,
  _,
  Handlebars,
  Backbone,
  NavigationHeaderView,
  NavigationFooterView,
  CreateSuccessTemplate,
  WaveGraphicView,
  ReviewListView,
  DrinkProgressView,
  StaticActionStepTemplate
) {

    "use strict";

    return Backbone.View.extend({

        initialize: function (options) {

          _.bindAll(this);
          window.addEventListener('resize', this.resizeAndRenderWave, false);

        },

        render: function (eventName) {
          console.log("Render success create view");
            var template  = Handlebars.compile(CreateSuccessTemplate);
            var data = {drinkName:this.model.get("name")}
            var html = template(data);
            this.$el.html(html);
            this.loadSubViews();

            return this;
        },

        /*----- Wave height logic -----*/
        resizeAndRenderWave: function() {
            console.log(App.recipe);
            var ingredients = App.recipe.recipeIngredients.getIngredientsOnly();
            var numIngredients = ingredients.length;

            //Calculate height of ingredient elements
            //Make sure to account for border heights
            var totalHeight = 1;
            var that = this;
            $(ingredients).each(function(index,element){
                var volume = element.get("volume");
                var type = element.get('type');
                var incrementUnit = App.initVolumeMapping[type];

                var height = 0 + parseFloat(volume / incrementUnit) * (44)+1;

                totalHeight+=height;
            });
            var height = totalHeight;

            //Resize other elements
            //this.$("#wave-wrapper").height = height-8;

            //Hardcoding actions bar height and instructions for now!
            //var minContentHeight = 55+192+height+8;
            //console.log(this.$("#actions-bar").height());
            //console.log(this.$("#actions-bar").length);

            //this.$("#recipe-steps-actions-wrapper").css("min-height",minContentHeight);

            //Important!
            this.$("#waveCanvas")[0].width = this.$("#wave-wrapper").width()-1;
            console.log(this.$("#waveCanvas").width());
            console.log(this.$("#wave-wrapper").width());


            this.waveGraphicView.renderWave(height-8);
        },

        postShowSetup: function()
        {
            this.resizeAndRenderWave();
        },

        loadSubViews: function() {
          console.log("Create success view load subviews");

            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Create Your Drink (4/4)"
            });
            this.navigationHeaderView.render();

            this.reviewListView = new ReviewListView({
                el: this.$("#recipe-steps-list-wrapper"),
                model: App.recipe
            });
            this.reviewListView.render();


            this.drinkProgressView = new DrinkProgressView({
                el: this.$("#drink-progress-wrapper"),
                model: App.recipe
            });
            this.drinkProgressView.render();
            this.drinkProgressView.animate();

            //WaveGraphicView
            this.waveGraphicView = new WaveGraphicView({
                el: this.$("#wave-wrapper")
            });

            this.waveGraphicView.render();
            this.resizeAndRenderWave();

            //Append action and strain by hand
            this.$("#recipe-actions-list-wrapper").empty();

            if(App.recipe.actionIngredient||App.recipe.strainIngredient)
            {
                this.$("#recipe-actions-list-wrapper").empty();
                this.$("#recipe-actions-list-wrapper").append("<div class='small-instructions-2'><p>Preparation method:</p></div>");
                this.$("#recipe-actions-list-wrapper").append("<ul id='action-steps-list' data-role='listview' data-inset='true' data-corners='false'></ul>");

                if(App.recipe.actionIngredient)
                {
                    var template  = Handlebars.compile(StaticActionStepTemplate);
                    var data = App.recipe.actionIngredient.toJSON();
                    var html = template(data);
                    this.$("#action-steps-list").append(html);
                }

                if(App.recipe.strainIngredient)
                {
                    var template  = Handlebars.compile(StaticActionStepTemplate);
                    var data = App.recipe.strainIngredient.toJSON();
                    var html = template(data);
                    this.$("#action-steps-list").append(html);
                }


            }

            ////////////////////////////
            /* Footer navigation here */
            ////////////////////////////

            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    false,
                nextButtonTitle:    "Order this drink",
                nextAllowed:        true,
                nextHref:           "#order" 
            });
            this.navigationFooterView.render();

            ///////////////////////////
            /* End footer navigation */
            ///////////////////////////


            //this.NextButtonView.render();
        },

    });

});