define([
    // Libs
    'jquery',
    'jquerymobile',
    'underscore',
    'handlebars',
    'backbone',
    'iscroll',

    // Views
    'views/common/NavigationHeaderView',
    'views/design/IngredientsScrollMenuView',
    'views/design/IngredientTypesScrollMenuView',
    'views/design/IngredientsListView',
    'views/common/NavigationFooterView',
    'views/design/WaveGraphicView',
    'views/design/DrinkProgressView',

    // Templates
    'text!templates/design/ingredientsCompositeTemplate.html',

    // Collections
    'collections/ingredients/IngredientsCollection',

    // Models
    'models/step/IngredientStep',

    'models/step/Step',
    'models/recipe/RecipeModel',
], function (
    $,
    jquerymobile,
    _,
    Handlebars,
    Backbone,
    iScroll,

    NavigationHeaderView,
    IngredientsScrollMenuView,
    IngredientTypesScrollMenuView,
    IngredientsListView,
    NavigationFooterView,
    WaveGraphicView,
    DrinkProgressView,

    IngredientsCompositeTemplate,

    IngredientsCollection,
    IngredientStep,
    Step,
    Recipe
) {

    "use strict";

    return Backbone.View.extend({

        removeHandler: function () {

            console.log('Kill: ', this);

            this.unbind(); // Unbind all local event bindings
            this.remove(); // Remove view from DOM

            //window.removeEventListener('resize', this.resizeAndRenderWave, false);

        },

        initialize: function (options) {
            console.log("Init IngredientsCompositeView");
            _.bindAll(this);

            //Assign ingredients passed from router at initialization
            //this.ingredients = this.options.ingredients;

            //NEW APPROACH --> Attach ingredients to App so we can access everywhere
            this.ingredients = App.ingredients;


             this.$el.on('teardown', this.removeHandler);

        /*----- Handlebars helpers -----*/

            //DEPRECATED!

            //Check if ingredient is a normal ingredient or an action ingredient
            /*
            Handlebars.registerHelper("ifIngredient", function (item,block) {
                console.log(this.type);
                return this.type != App.actionsIndex ? block.fn(this) : block.inverse(this);
            });

            //Check if we need an action placeholder
            Handlebars.registerHelper("ifNeedPlaceholderAction", function (item, options) {
                var checkIndex          = options.hash["index"];
                checkIndex              = parseInt(checkIndex)+1;
                var needsPlaceholder    = true;

                //If the next ingredient in the recipe is an action
                //we don't need an action placeholder
                if (checkIndex < that.model.recipeIngredients.length) {
                    var nextStep = that.model.recipeIngredients.at(checkIndex);
                    if (parseInt(nextStep.get("type")) == App.actionsIndex) {
                        needsPlaceholder = false;
                    }
                }
                return needsPlaceholder == true ? options.fn(this) : options.inverse(this);
            });*/

        /*----- Listen to model and update wave and footer accordingly -----*/
            this.listenTo(this.model.recipeIngredients, 'change', this.updateUI);
            this.listenTo(this.model.recipeIngredients, 'add', this.updateUI);
            this.listenTo(this.model.recipeIngredients, 'remove', this.updateUI);

            //important!
            //window.addEventListener('resize', this.resizeAndRenderWave, false);
        },


        render: function (eventName) {
            console.log("Render IngredientsCompositeView");
            var template  = Handlebars.compile(IngredientsCompositeTemplate);
            this.$el.html(template);
            this.loadSubViews();
            this.updateUI();
            return this;
        },


    /*----- Post show setup -----*/
        //Called by router after JQM pageshow event is triggered
        //View has been loaded into DOM and transition animation is complete
        //iScroll requires elements to be loaded and visible to calculate dimensions
        postShowSetup: function()
        {
            console.log("Post show setup");
            var that = this;

            //If we don't have any ingredients loaded,
            //Grab them from the server
            if(this.ingredients.length==0)
            {
                 //Show loading UI
                var theme = "a",
                    msgText = "Loading ingredients",
                    textVisible = true,
                    textonly = false,
                    html = "";
                $.mobile.loading("show", {
                    text: msgText,
                    textVisible: textVisible,
                    theme: theme,
                    textonly: textonly,
                    html: html
                });

                //Load ingredients and setup scroll menus
                this.ingredients.fetch({
                    //Handle success
                    success: function(data, textStatus, jqXHR)
                    {
                        console.log(textStatus);
                        //Validate true success
                        if(textStatus
                            && parseInt(textStatus.code)==1
                            && textStatus.ingList.length>0)
                        {
                            console.log("Success loading ingredients");
                            $.mobile.loading("hide"); //Hide loading UI

                            //Dispatch error event
                            //window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");


                            that.loadTypesMenu();
                            that.setIngredientsMenu(1);

                            //that.waveGraphicView.render();
                            //Update UI --> footer navigation and wave
                            that.updateUI();
                            //window.EventDispatcher.trigger("errorPopUp", "<== Scroll ==> \n Explore and add different ingredients to your drink!");

                            //that.waveGraphicView.renderWave(0);

                        } else
                        {
                            //Dispatch error event
                            window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                        }

                    },

                    //Handle error
                    error: function( jqXHR, textStatus, errorThrown)
                    {
                        //Dispatch error event
                        window.EventDispatcher.trigger("errorPopUp", "Error completing request. Please try again.");
                    }

                });
            } else
            {
                that.loadTypesMenu();
                    that.setIngredientsMenu(1);

                    //that.waveGraphicView.render();
                    //Update UI --> footer navigation and wave
                    that.updateUI();
                    //that.waveGraphicView.renderWave(0);
            }




        },


    /*----- Scrollview menu functions -----*/
        //Load ingredient types menu
        loadTypesMenu: function () {

            var types = App.ingredientsMapping.slice(); //new array
            $.each(types,function(index, value){
                //Remove actions
                if (value.typeId == App.actionsIndex)
                {
                    types.splice(index, 1);
                    return false;
                }
            })

            var data = { "items": types };

            var el = this.$("#types-scroll-container");

            this.ingredientTypesScrollMenuView = new IngredientTypesScrollMenuView({
                el: el,
                data: data
            });

            var placeholderEl = $("#design-menu-placeholder");

            if (placeholderEl.length === 0) {
                placeholderEl = $('#scroll-wrapper');
            }

        },

        setIngredientsMenu: function (ingredientType) {
            var type = parseInt(ingredientType);
            //Style the tabs
            $(".ingredient-type.tab").each(function(){
                var elementIngredientType = $(this).data("ingredient-type-id");
                elementIngredientType = parseInt(elementIngredientType);

                if(elementIngredientType!=type)
                {
                    $(this).removeClass("selected");
                } else
                {
                    $(this).addClass("selected");
                }
            });

            //Initialize ingredients scroll menu
            var items = this.ingredients.getType(ingredientType);
            var data = { "items": items };

            this.ingredientsScrollMenuView = new IngredientsScrollMenuView({
                el: this.$("#ingredients-scroll-container"),
                data: data
            });

        },




     /*----- User interface updates -----*/
        updateUI: function() {
            console.log("IngredientsCompositeView updateUI");
        /*----- Bottom navigation logic -----*/

            //Enable next only if recipe is set
            if (this.model.recipeIngredients.length==0)
            {
                this.navigationFooterView.nextAllowed = false; //Hide next button
                this.$("#instructions").show(); //Show instructions
            } else
            {
                this.navigationFooterView.nextAllowed = true; //Show next button
                this.$("#instructions").hide(); //Hide instruction

            }
            this.navigationFooterView.render();

            this.drinkProgressView.animate();

            //Resize the wave
            //this.resizeAndRenderWave();

        },

    /*----- Wave height logic -----*/
        resizeAndRenderWave: function() {
            var ingredients = this.model.recipeIngredients.getIngredientsOnly();
            var numIngredients = ingredients.length;

            //Calculate height of ingredient elements
            //Make sure to account for border heights
            var totalHeight = 1;
            var that = this;
            $(ingredients).each(function(index,element){

                var volume = element.get("volume");
                var type = element.get('type');
                var incrementUnit = App.initVolumeMapping[type];
                var height = 0 + parseFloat(volume / incrementUnit) * (44)+1;

                totalHeight+=height;
            });
            var height = totalHeight;

            //Resize other elements
            this.$("#wave-wrapper").height = height-8;
            var minContentHeight = this.$("#scroll-wrapper").height()+this.$("#types-scroll-container").height()+height+8;

            //this.$("#ingredients-content-wrapper").css("min-height",minContentHeight);

            //Important!
            this.$("#waveCanvas")[0].width = this.$("#wave-wrapper").width()-1;
            console.log(this.$("#waveCanvas").width());
            console.log(this.$("#wave-wrapper").width());

            this.waveGraphicView.renderWave(height-8);
        },


    /*----- Load subviews -----*/
        loadSubViews: function() {
            console.log("Load IngredientsCompositeView subviews");

            //Navigation header
            this.navigationHeaderView = new NavigationHeaderView({
                el: this.$("#navigation-header-wrapper"),
                title: "Add Ingredients (1/3)",
                nav: "design"
            });
            this.navigationHeaderView.render();

            this.drinkProgressView = new DrinkProgressView({
                el: this.$("#drink-progress-wrapper"),
                model: this.model
            });
            this.drinkProgressView.render();


            //Ingredients list
            this.ingredientsListView = new IngredientsListView({
                el: this.$("#ingredients-list-wrapper"),
                model: this.model
            });
            this.ingredientsListView.render();

            //WaveGraphicView
            //this.waveGraphicView = new WaveGraphicView({
            //    el: this.$("#wave-wrapper")
            //});


            //Navigation footer
            var nextAllowed = false;
            if (this.model.recipeIngredients.length>0)
            {
                nextAllowed = true;
            }

            this.navigationFooterView = new NavigationFooterView({
                el: this.$("#navigation-footer-wrapper"),
                needsBackButton:    false,
                nextButtonTitle:    "Next",
                nextAllowed:        nextAllowed,
                nextHref:           "#steps"
            });
            this.navigationFooterView.render();
        },

    /*----- Events -----*/
        events: {
            "click        .tab"                   :"switchTab",
            "click        .ingredient"            :"addIngredient",
            "click        .increase-ingredient"   :"increaseIngredient",
            "click        .decrease-ingredient"   :"decreaseIngredient",
            "click        .remove-ingredient"     :"removeIngredient",
        },

        addIngredient: function (event) {
            //Get ingredient ID
            var ingredientID = $(event.currentTarget).data("ingredient-id");
            ingredientID = parseInt(ingredientID);

            //Only add ingredient if recipe doesn't already contain it
            if (!this.model.recipeIngredients.doesContainIngredient(ingredientID)) {

                var actionIngredient = this.model.actionIngredient;


                if(actionIngredient
                    && parseInt(actionIngredient.get("ingredientId"))==App.muddleId
                    && ingredientID==App.iceId)
                {
                    window.EventDispatcher.trigger("errorPopUp", "Sorry, cannot add ice when you've chose to muddle your drink.");
                    return false;

                }



                var ingredient = this.ingredients.getIngredient(ingredientID);

                var newIngredientStep = new IngredientStep({
                    ingredientName:     ingredient.get("ingredientName"),
                    price:              ingredient.get("price"),
                    ingredientId:       ingredient.get("ingredientId"),
                    type:               ingredient.get("type")});

                var canIncrease = this.model.canIncreaseIngredient(ingredient.get("type"), ingredient.get("ingredientId"));
                if (!canIncrease) {
                    window.EventDispatcher.trigger("errorPopUp", "Not enough room for this ingredient. Please remove something to make room.");
                    event.preventDefault(); //Prevent linking
                    return false;
                }

                var canAddAlcohol = this.model.isBelowAlcoholLimit(ingredient.get("type"));
                if (!canAddAlcohol) {
                    window.EventDispatcher.trigger("errorPopUp", "Bummer... We can only make drinks with up to 2 shots (88 ml) of alcohol.");
                    event.preventDefault(); //Prevent linking
                    return false;
                }


                var recipeIngredients = this.model.recipeIngredients;
                recipeIngredients.add(newIngredientStep);
            } else
            {
                //Dispatch error event
                window.EventDispatcher.trigger("errorPopUp", "You've already added this ingredient.");
            }
            event.preventDefault(); //Prevent linking
        },

        removeIngredient: function (event) {
            //Find the index of the list item that contains the event target (button)
            var ingredientIndex = $(event.target).parents("li.added-ingredient").data("step-number");
            ingredientIndex = parseInt(ingredientIndex);

            //Prevent access outside array
            if (ingredientIndex >= 0 && ingredientIndex < this.model.recipeIngredients.length) {
                var ingredient = this.model.recipeIngredients.at(ingredientIndex);

                this.model.recipeIngredients.remove(ingredient);

                //Also remove any mix step immediately below ingredient
                if (ingredientIndex < this.model.recipeIngredients.length) {
                    var ingredient = this.model.recipeIngredients.at(ingredientIndex);
                    var type = ingredient.get("type");
                    if (type == App.actionsIndex) {
                        this.model.recipeIngredients.remove(ingredient);
                    }
                }
            }
            event.preventDefault(); //Prevent linking
        },

        increaseIngredient: function (event) {
            var ingredientIndex = $(event.target).parents("li.added-ingredient").data("step-number");
            var element = $(event.target).parents("li.added-ingredient");

            ingredientIndex = parseInt(ingredientIndex);



            //Prevent access outside array
            if (ingredientIndex >= 0 && ingredientIndex < this.model.recipeIngredients.length) {
                var ingredient = this.model.recipeIngredients.at(ingredientIndex);

                var type = ingredient.get('type');
                var incrementUnit = App.incrementVolumeMapping[type];

                var canIncrease = this.model.canIncreaseIngredient(ingredient.get("type"), ingredient.get("ingredientId"));

                console.log("canIncrease:", canIncrease);
                if (!canIncrease) {
                    window.EventDispatcher.trigger("errorPopUp", "Not enough room for this ingredient. Please remove something to make room.");
                    event.preventDefault(); //Prevent linking
                    return false;
                }

                var canAddAlcohol = this.model.isBelowAlcoholLimit(ingredient.get("type"));

                console.log("canAddAlcohol:", canAddAlcohol);
                if (!canAddAlcohol) {
                    window.EventDispatcher.trigger("errorPopUp", "Bummer... We can only make drinks with up to 2 shots (88 ml) of alcohol.");
                    event.preventDefault(); //Prevent linking
                    return false;
                }

                var newVolume = ingredient.get("volume") + incrementUnit;
                var volume = ingredient.get("volume");
                ingredient.set({volume: newVolume});
            }

            event.preventDefault(); //Prevent linking
        },

        decreaseIngredient: function (event) {

            var ingredientIndex = $(event.target).parents("li.added-ingredient").data("step-number");
            ingredientIndex = parseInt(ingredientIndex);

            //Prevent access outside array
            if (ingredientIndex >= 0 && ingredientIndex < this.model.recipeIngredients.length) {
                var ingredient = this.model.recipeIngredients.at(ingredientIndex);

                var type = ingredient.get('type');
                var minUnit       = App.initVolumeMapping[type];
                var incrementUnit = App.incrementVolumeMapping[type];

                var newVolume = ingredient.get("volume") - incrementUnit;
                if (newVolume < minUnit) {
                  // newVolume = minUnit;
                    this.removeIngredient(event);
                }
                var volume = ingredient.get("volume");
                ingredient.set({volume: newVolume});
            }

            event.preventDefault(); //prevent linking
        },

        switchTab: function (event) {
            console.log("SwitchTab");
            var ingredientType = $(event.currentTarget).data("ingredient-type-id");
            ingredientType = parseInt(ingredientType);
            //Validate that we have an ingredient type
            //And prevent access outside array
            if(ingredientType
                && ingredientType >= 0
                && ingredientType < this.ingredients.length)
            {
                this.setIngredientsMenu(ingredientType);
            }

            event.preventDefault(); //prevent linking
        },



    });

});