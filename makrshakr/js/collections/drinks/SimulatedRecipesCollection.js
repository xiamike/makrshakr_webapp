define([
    'jquery',
    'underscore',
    'backbone',
    'models/recipe/RecipeModel'
], function ($, _, Backbone, RecipeModel) {

    "use strict";

    return Backbone.Collection.extend({

        //specify model
        model: RecipeModel,

        //specify url endpoint
        url: function () {
            return 'data/simulatedRecipes.json';
        },

        //specify root to models
        parse: function (resp, xhr) {
            return resp.recipes;
        }
    
    });

});