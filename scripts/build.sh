r.js -o build.js
cd ../js-min

printf "Removing collections folder \n"
rm -rf collections

printf "Removing lib folder \n"
rm -rf libs

printf "Removing models folder \n"
rm -rf models

printf "Removing views folder \n"
rm -rf views

printf "Removing wrapper folder \n"
rm -rf wrapper

printf "Removing debugger and text \n"
rm -rf Router.js
rm -rf text.js

printf "Removing JS lib files \n"
# rm -rf lib
#remove the require.js file from the lib folder then put it back in once everything is deleted
mv libs/require.js require.js && rm -rf libs/* && mv require.js libs/require.js

printf "All done! \n"

